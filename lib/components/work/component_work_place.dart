import 'package:flutter/material.dart';
import 'package:my_worker_app/config/color.dart';

class ComponentWorkPlace extends StatefulWidget {
  const ComponentWorkPlace({super.key});

  @override
  State<ComponentWorkPlace> createState() => _ComponentWorkPlaceState();
}

class _ComponentWorkPlaceState extends State<ComponentWorkPlace> {
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      //제목
      Column(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              '매장정보',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
          )
        ],
      ),
      //내용
      Column(
        children: [
          Row(
            children: [
              Container(
                child: Icon(
                  Icons.store_mall_directory_rounded,
                  size: 100,
                  color: colorNormal,
                ),
              ),
              Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width/1.1-100,
                    alignment: Alignment.centerLeft,
                    child: Text('동대문 엽기 떡볶이 부곡점'),
                  ),

                  Container(
                    width: MediaQuery.of(context).size.width/1.1-100,
                    alignment: Alignment.centerLeft,
                    child: Text('대표자명 : 나사장'),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width/1.1-100,
                    alignment: Alignment.centerLeft,
                    child: Text('업종 : 일반 음식점'),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width/1.1-100,
                    alignment: Alignment.centerLeft,
                    child: Text('전화번호 : 031-485-2483'),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width/1.1-100,
                    alignment: Alignment.centerLeft,
                    child: Text('주소 : 경기도 안산시 단원구 고잔동 534-3번지 130호'),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width/1.1-100,
                    alignment: Alignment.centerLeft,
                    child: Text('상세주소 : 안산중앙 노블레스 220호'),
                  ),
                ],
              )
            ],
          ),
        ],
      )
    ]);
  }
}
