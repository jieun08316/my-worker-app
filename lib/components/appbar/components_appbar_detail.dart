import 'package:flutter/material.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';

enum SampleItem { itemOne, itemTwo }

class ComponentsAppbarDetail extends StatelessWidget
    implements PreferredSizeWidget {
  const ComponentsAppbarDetail({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: BackButton(
        color: Colors.white,
      ),
      title: Text(
        title,
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      centerTitle: true,
      actions: [
        PopupMenuButton(
          child: ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: Image.asset(
                'assets/img/three_dots.png',
                color: Colors.white,
                width: 26,
                height: 26,
              )),
          color: Colors.white,
          onSelected: (value) {
            if (value == "수정하기") {
              // add desired output
            } else if (value == "삭제하기") {
              showDialog<String>(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                  title: const Text(
                    '삭제하시겠습니까?',
                    style: TextStyle(color: colorBaseBlack),
                  ),
                  // content: const Text('AlertDialog description'),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context, 'Cancel'),
                      child: const Text(
                        '취소',
                        style: TextStyle(color: colorNormal),
                      ),
                    ),
                    TextButton(
                      onPressed: () => Navigator.pop(context, 'OK'),
                      child: const Text('확인',
                          style: TextStyle(color: colorNormal)),
                    ),
                  ],
                  backgroundColor: Colors.white,
                ),
              );
              // add desired output
            }
          },
          itemBuilder: (BuildContext context) => <PopupMenuEntry>[
            PopupMenuItem(
              value: "수정하기",
              child: Row(
                children: [
                  Container(
                    margin: const EdgeInsets.only(right: 10.0),
                    child: Image.asset(
                      'assets/img/pencil.png',
                      color: colorBaseBlack,
                      width: 25,
                      height: 25,
                    ),
                  ),
                  const Text(
                    '수정하기',
                    style: TextStyle(fontSize: 15, color: colorBaseBlack),
                  ),
                ],
              ),
            ),
            PopupMenuItem(
              value: "삭제하기",
              child: Row(
                children: [
                  Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Image.asset(
                        'assets/img/delete.png',
                        color: colorBaseBlack,
                        width: 25,
                        height: 25,
                      )),
                  const Text(
                    '삭제하기',
                    style: TextStyle(fontSize: 15, color: colorBaseBlack),
                  ),
                ],
              ),
            ),
          ],
        )
      ],
      backgroundColor: colorNormal,
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(50);
}
