import 'package:flutter/material.dart';
import 'package:my_worker_app/config/color.dart';

class ComponentAppbarBase extends StatelessWidget
    implements PreferredSizeWidget {
  const ComponentAppbarBase({super.key, required this.title});

  final String title;

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      iconTheme: IconThemeData(
        color: Colors.white, //뒤로가기 버튼 색상 변경
      ),
      centerTitle: true,
      backgroundColor: colorNormal,
      title: Text(
        title,
        style: TextStyle(
          color: Colors.white,
          fontSize: 20.0,
          fontWeight: FontWeight.w900,
          shadows: [
            Shadow(
              blurRadius: 5.0,
              color: Color.fromRGBO(63, 114, 175, 0.2),
            ),
          ],
        ),
      ),
      elevation: 2.0,
      // 앱바 아래 그림자처럼 보이는 것

      actions: [
        Container(
          child: Image.asset(
            'assets/img/logo_t.png',
            width: 50,
            height: 50,
          ),
        )
      ],
      shadowColor: Colors.grey,
    );
  }
}
