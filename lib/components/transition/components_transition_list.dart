import 'package:flutter/material.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/home/transition/transition_list.dart';
import 'package:my_worker_app/model/home/transition/transition_list.dart';
import 'package:my_worker_app/pages/main/store_main/transition/page_transition_put.dart';
import 'package:my_worker_app/repository/my_page/repo_transition.dart';

class ComponentsTransitionList extends StatefulWidget {
  const ComponentsTransitionList({
    super.key,
    required this.transitionList
  });

  final TransitionList transitionList;

  @override
  State<ComponentsTransitionList> createState() =>
      _ComponentsTransitionListState();
}

class _ComponentsTransitionListState
    extends State<ComponentsTransitionList> {

  Future<void> _putButton() async {
    await RepoTransition().putButton(widget.transitionList.id)
        .then((res) => {
      setState(() {
        changeText();
      })
    });
  }

  String _buttonState = '미처리';
  var _color = colorDark;
  var _textColor = Colors.black26;

  void changeText() {
    setState(() {
      // 미처리 == 미처리
      if (_buttonState == '미처리') {
        _buttonState = '처리완료';
        _color = colorNormal;
        _textColor = colorDark;
      }

      else {
        _buttonState = '미처리';
        _color = colorDark;
        _textColor = Colors.black26;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
          children: [
            Container(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            child: Column(
                              children: [
                                Text(
                                  '${widget.transitionList.memberName}',
                                  style: TextStyle(
                                    fontSize: fontSizeMedium,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                            margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                          ),
                          Text(
                            '시급제 알바생',
                            style: TextStyle(
                                fontSize: fontSizeSmall,
                                color: colorGrey),
                          )
                        ],
                      ),
                      Container(
                        child: Row(
                          children: [
                            _putTransition(),
                            _isFinishButton(),
                          ],
                        ),
                        margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                      ),

                    ],
                  ),
                  Row(
                    children: [
                      Text('${widget.transitionList.content}'),
                    ],
                  )
                ],
              ),
              margin: EdgeInsets.fromLTRB(20, 15, 0, 5),
            ),
            Divider(
                color: colorLight
            ),
          ],
        ),
        width: MediaQuery
            .of(context)
            .size
            .width / 1.2,
    );
  }

  Widget _isFinishButton () {
    if (widget.transitionList.isFinish == '미처리') {
      return Container(
        width: 80,
        child: ElevatedButton(
          child: Text('${widget.transitionList.isFinish}',
            style: TextStyle(
                fontSize: 12, color: _textColor, height: 1.5),
          ),
          onPressed: _putButton,
          style: ElevatedButton.styleFrom(
              side: BorderSide(color: _color, width: 2,
              ),
              padding: EdgeInsets.zero
          ),
        ),
      );
    } else {
      return Container(
        width: 85,
        child: Container(
          child: ElevatedButton(
            child: Text('${widget.transitionList.isFinish}',
              style: TextStyle(
                  fontSize: 12, color: colorDark, height: 1.5),
            ),
            onPressed: _putButton,
            style: ElevatedButton.styleFrom(
                side: const BorderSide(color: colorNormal, width: 2,
                ),
                padding: EdgeInsets.zero
            ),
          ),
        ),
      );
    }
  }

  Widget _putTransition () {
    if (widget.transitionList.isFinish == '처리 완료') {
      return Container(
        width: 85,
        margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
        child: ElevatedButton(
          child: Text('수정 불가',
            style: TextStyle(
                fontSize: 12, color: colorDark, height: 1.5),
          ),
          onPressed: null,
          style: ElevatedButton.styleFrom(
              side: const BorderSide(color: colorNormal, width: 2,
              ),
            padding: EdgeInsets.zero
          ),
        ),
      );
    } else {
      return Container(
        width: 80,
        margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
        child: ElevatedButton(
          child: Text('수정',
            style: TextStyle(
                fontSize: 12, color: colorDark, height: 1.5),
          ),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => PageTransitionPut(id: widget.transitionList.id,))
            );
          },
          style: ElevatedButton.styleFrom(
              side: const BorderSide(color: colorNormal, width: 2,
              ),
              padding: EdgeInsets.zero
          ),
        ),
      );
    }


  }

}
