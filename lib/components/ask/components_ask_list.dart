import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:my_worker_app/model/home/ask/ask_list.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';

class ComponentsAskList extends StatelessWidget {
  const ComponentsAskList({
    super.key,
    required this.callback,
    required this.askList,
  });

  final VoidCallback callback;
  final AskList askList;

  @override
  Widget build(BuildContext context) {

    DateTime dateTime = DateTime.parse(askList.dateMemberAsk);

    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Container(
              child: Row(
                children: [
                  Container(
                    child: Column(
                      children: [
                        Image.asset('assets/img/logo_t.png', width: 80, height: 80,),
                        // Text(askList.questionImgUrl),
                      ],
                    ),
                    margin: EdgeInsets.fromLTRB(0, 0, 20, 0),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('${askList.title}', style: TextStyle(fontSize: fontSizeMedium),),
                      Row(
                        children: [
                          Text('작성 날짜: ', style: TextStyle(color: Color.fromRGBO(40, 40, 40, 1.0), fontSize: 13),),
                          Container(
                            child: Text('${DateFormat('y년 M월 d일').format(dateTime)}', style: TextStyle(color: Color.fromRGBO(40, 40, 40, 1.0), fontSize: 13),),
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            width: 160,
                          ),
                        ],
                      ),
                      Text('${askList.isAnswer}', style: TextStyle(color: colorNormal, fontSize: 13),),
                    ],
                  ),
                ],
              ),
              margin: EdgeInsets.fromLTRB(20, 10, 0, 5),
            ),
            Divider(
              color: colorLight,
            ),
          ],
        ),
        width: MediaQuery.of(context).size.width / 1.0,
      ),
    );
  }
}
