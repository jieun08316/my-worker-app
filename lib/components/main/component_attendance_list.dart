import 'package:flutter/material.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/main/my_work_attendance_list_item.dart';

class ComponentAttendanceList extends StatefulWidget {
  const ComponentAttendanceList(
      {super.key, required this.myWorkAttendanceListItem});

  final MyWorkAttendanceListItem myWorkAttendanceListItem;

  @override
  State<ComponentAttendanceList> createState() =>
      _ComponentAttendanceListState();
}

class _ComponentAttendanceListState extends State<ComponentAttendanceList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      // ListView.builder을 선언하기 전에 먼저 높이를 설정해줘서 오류를 피해줍니다.
      child: Row(
        // Image.asset 다른 옵션들로 크기 등 조정할 수 있습니다
        children: [
          Column(children: [
            Container(
              child: Text(
                  '${widget.myWorkAttendanceListItem.businessMemberPosition}'),
            ),
            Container(
              child:
                  Text('${widget.myWorkAttendanceListItem.businessMemberName}'),
            ),
            Column(
              children: [
                Container(
                    width: MediaQuery.of(context).size.width / 5,
                    child: Text(
                      '출근시간 : ${widget.myWorkAttendanceListItem.startWork}',
                      textAlign: TextAlign.center,
                    )),
                Container(
                  width: MediaQuery.of(context).size.width / 5,
                  child: Text(
                    '퇴근시간 : ${widget.myWorkAttendanceListItem.startWork}',
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 5,
                  child: Text(
                    '총 근무시간  : ${widget.myWorkAttendanceListItem.totalWorkTime}',
                  ),
                )
              ],
            ),
            Column(
              children: [
                Column(
                  children: [
                    Container(
                      child: Text('출근/지각 여부'),
                    ),
                    Container(
                      child:
                          Text('${widget.myWorkAttendanceListItem.isLateness}'),
                    ),
                  ],
                ),
                Container(
                  child: Text('날짜 ${widget.myWorkAttendanceListItem.dateWork}'),
                )
              ],
            )
          ]),
          Divider(
            color: colorGrey,
            thickness: 1,
          )
        ],
      ),
    );
  }
}

/// 컴포넌트 연동 보류*
///
///  // Container(
//     //       child: Row(
//     //         // Image.asset 다른 옵션들로 크기 등 조정할 수 있습니다
//     //         children: [
//     //           Column(children: [
//     //             Container(
//     //               child: Text(
//     //                   '알바생'),
//     //             ),
//     //             Container(
//     //               child: Text(
//     //                   '나알바'),
//     //             ),
//     //             Column(
//     //               children: [
//     //                 Container(
//     //                     width: MediaQuery.of(context).size.width / 5,
//     //                     child: Text(
//     //                       '출근시간 : 09:00',
//     //                       textAlign: TextAlign.center,
//     //                     )),
//     //                 Container(
//     //                   width: MediaQuery.of(context).size.width / 5,
//     //                   child: Text(
//     //                     '퇴근시간 : 06:00',
//     //                   ),
//     //                 ),
//     //                 Container(
//     //                   width: MediaQuery.of(context).size.width / 5,
//     //                   child: Text(
//     //                     '총 근무시간  : 8시간',
//     //                   ),
//     //                 )
//     //               ],
//     //             ),
//     //             Column(
//     //               children: [
//     //                 Column(
//     //                   children: [
//     //                     Container(
//     //                       child: Text('출근/지각 여부'),
//     //                     ),
//     //                     Container(
//     //                       child: Text(
//     //                           '정상출근'),
//     //                     ),
//     //                   ],
//     //                 ),
//     //                 Container(
//     //                   child: Text(
//     //                       '날짜 2024-05-05'),
//     //                 )
//     //               ],
//     //             )
//     //           ]),
//     //           Divider(
//     //             color: colorGrey,
//     //             thickness: 1,
//     //           )
//     //         ],
//     //
//     //
//     //   ),
//     // );
// class ComponentAttendanceList extends StatefulWidget {
//   const ComponentAttendanceList(
//       {super.key, required this.myWorkAttendanceListItem});
//
//   final MyWorkAttendanceListItem myWorkAttendanceListItem;
//
//   @override
//   State<ComponentAttendanceList> createState() =>
//       _ComponentAttendanceListState();
// }
///
///
//  Container(
//       height: MediaQuery.of(context).size.height,
//       // ListView.builder을 선언하기 전에 먼저 높이를 설정해줘서 오류를 피해줍니다.
//       child: ListView.builder(
//         shrinkWrap: true,
//         // 현재 표시되는 아이템에 맞게 크기를 자동 조절
//         padding: EdgeInsets.all(5),
//
//         // 몇번을 수행할것인가에 대해서 미리 생성해둔 imageList의 길이를 가져오는 부분
//         itemCount: 1000,
//         itemBuilder: (BuildContext context, int index) {
//           return Row(
//             // Image.asset 다른 옵션들로 크기 등 조정할 수 있습니다
//             children: [
//               Column(children: [
//                 Container(
//                   child: Text(
//                       '${widget.myWorkAttendanceListItem.businessMemberPosition}'),
//                 ),
//                 Container(
//                   child: Text(
//                       '${widget.myWorkAttendanceListItem.businessMemberName}'),
//                 ),
//                 Column(
//                   children: [
//                     Container(
//                         width: MediaQuery.of(context).size.width / 5,
//                         child: Text(
//                           '출근시간 : ${widget.myWorkAttendanceListItem.startWork}',
//                           textAlign: TextAlign.center,
//                         )),
//                     Container(
//                       width: MediaQuery.of(context).size.width / 5,
//                       child: Text(
//                         '퇴근시간 : ${widget.myWorkAttendanceListItem.startWork}',
//                       ),
//                     ),
//                     Container(
//                       width: MediaQuery.of(context).size.width / 5,
//                       child: Text(
//                         '총 근무시간  : ${widget.myWorkAttendanceListItem.totalWorkTime}',
//                       ),
//                     )
//                   ],
//                 ),
//                 Column(
//                   children: [
//                     Column(
//                       children: [
//                         Container(
//                           child: Text('출근/지각 여부'),
//                         ),
//                         Container(
//                           child: Text(
//                               '${widget.myWorkAttendanceListItem.isLateness}'),
//                         ),
//                       ],
//                     ),
//                     Container(
//                       child: Text(
//                           '날짜 ${widget.myWorkAttendanceListItem.dateWork}'),
//                     )
//                   ],
//                 )
//               ]),
//               Divider(
//                 color: colorGrey,
//                 thickness: 1,
//               )
//             ],
//           );
//         },
//       ),
//     );
