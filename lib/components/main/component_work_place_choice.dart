import 'package:flutter/material.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/main/my_work_place_item.dart';

class ComponentWorkPlaceChoice extends StatelessWidget {
  const ComponentWorkPlaceChoice(
      {super.key, required this.callback, required this.myWorkPlaceItem});

  final VoidCallback callback;
  final MyWorkPlaceItem myWorkPlaceItem;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: GestureDetector(
        onTap: callback,
          child: Container(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Text(
                    '${myWorkPlaceItem.businessName}',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.w500, fontSize: 16),
                  ),
                ),
                Text('[touch]',style: TextStyle(
                  fontSize: 10,color: Color.fromRGBO(
                    255, 255, 255, 0.3215686274509804)
                ),)
              ],
            ),
            alignment: Alignment.center,
            width: 100,
            height: 100,
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.3),
                  spreadRadius: 5,
                  blurRadius: 10,
                  offset: Offset(0, 2), // changes position of shadow
                ),
              ],
                border: Border.all(width: 5.0, color: colorNormal),
                color: colorNormal,
                borderRadius: BorderRadius.circular(80)),
            // margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          ),
      ),
    );
  }
}
