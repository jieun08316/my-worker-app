import 'package:flutter/material.dart';
import 'package:my_worker_app/config/color.dart';

class ComponentNullList extends StatefulWidget {
  const ComponentNullList({super.key});

  @override
  State<ComponentNullList> createState() => _ComponentNullListState();
}

class _ComponentNullListState extends State<ComponentNullList> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width / 1.1,
        height: 200,
        child: Column(
          children: [
            Center(
              child: const Text(
                '결과가 없습니다.',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  color: colorBaseBlack,
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 5),
              child: Center(
                child: TextButton(
                  onPressed: () {},
                  child: const Text(
                    '목록 [새로고침]',
                    style: TextStyle(
                        color: colorNormal,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
          ],
        ));
  }
}
