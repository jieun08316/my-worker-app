// import 'package:flutter/material.dart';
// import 'package:my_worker_app/model/home/home_work_place_info_item.dart';
//
// class ComponentWorkPlaceInfo extends StatefulWidget {
//   const ComponentWorkPlaceInfo({super.key,
//     required this.homeWorkPlaceInfoItem});
//
//
//   final HomeWorkPlaceInfoItem homeWorkPlaceInfoItem;
//
//   @override
//   State<ComponentWorkPlaceInfo> createState() => _ComponentWorkPlaceInfoState();
// }
//
// class _ComponentWorkPlaceInfoState extends State<ComponentWorkPlaceInfo> {
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Row(children: [
//         Container(child: Icon(Icons.business_sharp),),
//         Column(
//           children: [
//             Container(child: Text('${widget.homeWorkPlaceInfoItem.businessName}'),),
//             Container(child:Text('${widget.homeWorkPlaceInfoItem.ownerName}'),),
//             Container(child:Text('${widget.homeWorkPlaceInfoItem.businessType}'),),
//             Container(child:Text('${widget.homeWorkPlaceInfoItem.businessPhoneNumber}'),),
//             Container(child:Text('${widget.homeWorkPlaceInfoItem.reallyLocation}'),),
//           ],
//         )
//       ],),
//     );
//   }
// }
