import 'package:flutter/material.dart';
import 'package:my_worker_app/config/color.dart';

class ComponentNullItem extends StatefulWidget {
  const ComponentNullItem({super.key});

  @override
  State<ComponentNullItem> createState() => _ComponentNullItemState();
}

class _ComponentNullItemState extends State<ComponentNullItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width/1.1,
      height: MediaQuery.of(context).size.height/3-35,
      child:  Column(
        children: [
          Center(
            child: const Text(
              '등록된 근무지가 없습니다.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                color: colorBaseBlack,
              ),
            ),
          ),
          Center(
            widthFactor:
            MediaQuery.of(context).size.width /
                1.5,
            child: const Text(
              '해당 사업장 사업주에게\n'
                  '[근무자 등록하기]요청해 주세요.',
              textAlign: TextAlign.center,
              style: TextStyle(color: colorGrey,
              fontSize: 16),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 5),
            child: Center(
              child: TextButton(
                onPressed: () {},
                child: const Text(
                  '목록 [새로고침]',
                  style: TextStyle(
                      color: colorNormal,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
        ],
      )
    );
  }
}
