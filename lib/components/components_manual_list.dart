import 'package:flutter/material.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/home/manual/manual_list.dart';

class ComponentsManualList extends StatelessWidget {
  const ComponentsManualList({
    super.key,
    required this.callback,
    required this.manualList,
  });

  final VoidCallback callback;
  final ManualList manualList;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Container(
              child: Row(
                children: [
                  Container(
                    child: Image.asset(
                      'assets/img/logo_t.png',
                      width: 50,
                      height: 50,
                    ),
                    margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            '${manualList.title}',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                      Container(
                        child: Row(
                          children: [
                            Row(
                              children: [
                                Text(
                                  '작성 날짜: ',
                                  style: TextStyle(
                                    color: Color.fromRGBO(40, 40, 40, 1.0),
                                    fontSize: fontSizeSmall,
                                  ),
                                ),
                                Text(
                                  '${manualList.dateManual}',
                                  style: TextStyle(
                                    color: Color.fromRGBO(40, 40, 40, 1.0),
                                    fontSize: fontSizeSmall,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width / 10.5,
                            ),
                            Row(
                              children: [
                                Text(
                                  '작성자: ',
                                  style: TextStyle(
                                    color: Color.fromRGBO(40, 40, 40, 1.0),
                                    fontSize: fontSizeSmall,
                                  ),
                                ),
                                Text(
                                  '${manualList.memberName}',
                                  style: TextStyle(
                                    color: Color.fromRGBO(40, 40, 40, 1.0),
                                    fontSize: fontSizeSmall,
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                        margin: EdgeInsets.fromLTRB(0, 4, 0, 0),
                      ),
                    ],
                  ),
                ],
              ),
              margin: EdgeInsets.fromLTRB(20, 0, 0, 5),
            ),
            Divider(
              color: colorLight,
            ),
          ],
        ),
        margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
        width: MediaQuery.of(context).size.width / 1.2,
      ),
    );
  }
}
