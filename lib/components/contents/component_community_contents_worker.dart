import 'package:flutter/material.dart';

class ComponentCommunityContentsWorker extends StatefulWidget {
  const ComponentCommunityContentsWorker({super.key});

  @override
  State<ComponentCommunityContentsWorker> createState() =>
      _ComponentCommunityContentsWorkerState();
}

class _ComponentCommunityContentsWorkerState
    extends State<ComponentCommunityContentsWorker> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      // ListView.builder을 선언하기 전에 먼저 높이를 설정해줘서 오류를 피해줍니다.
      child: ListView.builder(
        shrinkWrap: true,

        // 현재 표시되는 아이템에 맞게 크기를 자동 조절
        padding: EdgeInsets.all(5),
        // 몇번을 수행할것인가에 대해서 미리 생성해둔 imageList의 길이를 가져오는 부분
        itemCount: 1000,
        itemBuilder: (BuildContext context, int index) {
          return Column(
            // Image.asset 다른 옵션들로 크기 등 조정할 수 있습니다
            children: [
              Row(
                children: [
                  Container(
                      width: MediaQuery.of(context).size.width / 5,
                      child: Text(
                        '제목',
                        textAlign: TextAlign.center,
                      )),
                  Container(
                    width: MediaQuery.of(context).size.width / 5,
                    child: Text('시간', textAlign: TextAlign.center),
                  )
                ],
              ),
              Container(
                child: Text('내용'),
              ),
              Row(
                children: [
                  Container(
                      width: MediaQuery.of(context).size.width / 5,
                      child: Text(
                        '댓글',
                        textAlign: TextAlign.center,
                      )),
                  Container(
                    width: MediaQuery.of(context).size.width / 5,
                    child: Text('배지', textAlign: TextAlign.center),
                  )
                ],
              ),
            ],
          );
        },
      ),
    );
  }
}
