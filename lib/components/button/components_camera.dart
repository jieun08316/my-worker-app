import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:form_builder_image_picker/form_builder_image_picker.dart';
import 'package:my_worker_app/config/color.dart';

class ComponentsCamera extends StatefulWidget {
  const ComponentsCamera({
    super.key,
    required this.name
  });

  final String name;

  @override
  State<ComponentsCamera> createState() => _ComponentsCameraState();
}

class _ComponentsCameraState extends State<ComponentsCamera> {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 9,
      width: MediaQuery.of(context).size.width / 2,
      child: FormBuilderImagePicker(
        iconColor: colorDark,
        backgroundColor: Colors.white,
        decoration: InputDecoration(
          border: InputBorder.none,
        ),
        name: widget.name,
        optionsBuilder: (cameraPicker, galleryPicker) =>
            CupertinoActionSheet(
              actions: [
                CupertinoActionSheetAction(
                  isDefaultAction: true,
                  onPressed: () {
                    cameraPicker();
                  },
                  child: const Text('사진 찍기', style: TextStyle(color: colorDark, fontWeight: FontWeight.w400),),
                ),
                CupertinoActionSheetAction(
                  isDefaultAction: true,
                  onPressed: () {
                    galleryPicker();
                  },
                  child: const Text('갤러리에서 가져오기', style: TextStyle(color: colorDark, fontWeight: FontWeight.w400),),
                )
              ],
            ),
      ),
    );
  }
}
