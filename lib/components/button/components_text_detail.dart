import 'package:flutter/material.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/config/size.dart';

class ComponentsTextDetail extends StatefulWidget {
  const ComponentsTextDetail({
    super.key,
    required this.title,
    required this.data,
    required this.margin
  });

  final String title;
  final String data;
  final EdgeInsetsGeometry? margin;

  @override
  State<ComponentsTextDetail> createState() => _ComponentsTextDetailState();
}

class _ComponentsTextDetailState extends State<ComponentsTextDetail> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Text(widget.title),
          Container(
            child: Text(widget.data,),
            margin: widget.margin
          )
        ],
      ),
      height: 40,
    );
  }
}
