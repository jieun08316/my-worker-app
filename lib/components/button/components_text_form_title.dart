import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/config/size.dart';

class ComponentsTextFormTitle extends StatefulWidget {
  const ComponentsTextFormTitle({
    super.key,
    required this.title,
    required this.name,
    required this.hintText,
    required this.maxLength,
    required this.margin,
    required this.height
  });

  final String title;
  final String name;
  final String hintText;
  final int maxLength;
  final EdgeInsetsGeometry? margin;
  final double? height;

  @override
  State<ComponentsTextFormTitle> createState() => _ComponentsTextFormTitleState();
}

class _ComponentsTextFormTitleState extends State<ComponentsTextFormTitle> {
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          widget.title,
          style: TextStyle(color: colorGrey, fontSize: fontSizeMedium),
        ),
        Container(
          child: FormBuilderTextField(
            name: widget.name,
            maxLength: widget.maxLength,
            decoration: InputDecoration(
              hintText: widget.hintText,
              hintStyle: TextStyle(height: 1.0),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
                borderSide: BorderSide(
                    color: colorDark,
                    width: 2),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
                borderSide: BorderSide(
                    color: colorNormal,
                    width: 2),
              ),
            ),
            textInputAction: TextInputAction.next,
            validator: (String? value) {
              if (value == null || value.isEmpty) {
                return '필수 값 입니다. 다시 확인해 주세요.';
              }
              return null;
            },
          ),
          width: MediaQuery.of(context).size.width / 1.7,
          height: widget.height,
          margin: widget.margin,
        ),
      ],
    );
  }
}
