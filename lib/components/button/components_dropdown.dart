import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/config/size.dart';

class ComponentsDropdown extends StatefulWidget {
  const ComponentsDropdown({
    super.key,
    required this.name,
    required this.initialValue,
    required this.suffix,
    required this.hintText,
    required this.items,
    required this.title,
    required this.margin,
    required this.width,
    required this.marginTitle,
  });

  final String name;
  final String initialValue;
  final Widget? suffix;
  final String hintText;
  final List<DropdownMenuItem<String>> items;
  final String title;
  final EdgeInsetsGeometry? margin;
  final double? width;
  final EdgeInsetsGeometry? marginTitle;

  @override
  State<ComponentsDropdown> createState() => _ComponentsDropdownState();
}

class _ComponentsDropdownState extends State<ComponentsDropdown> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: widget.margin,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            widget.title,
            style: TextStyle(
                color: colorGrey,
                fontSize: fontSizeMedium),
          ),
          Container(
            margin: widget.marginTitle,
            width: widget.width,
            height: MediaQuery.of(context).size.height / 12,
            child: DropdownButtonHideUnderline(
              child: FormBuilderDropdown<String>(
                name: widget.name,
                initialValue: widget.initialValue,
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide:
                    BorderSide(color: colorNormal, width: 2),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide:
                    BorderSide(color: colorDark, width: 2),
                  ),
                  suffix: widget.suffix,
                  hintText: widget.hintText,
                ),
                items: widget.items,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
