import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/config/size.dart';

class ComponentsTextFormTitleNumber extends StatefulWidget {
  const ComponentsTextFormTitleNumber({
    super.key,
    required this.title,
    required this.name,
    required this.hintText,
    required this.margin,
  });

  final String title;
  final String name;
  final String hintText;
  final EdgeInsetsGeometry? margin;

  @override
  State<ComponentsTextFormTitleNumber> createState() => _ComponentsTextFormTitleNumberState();
}

class _ComponentsTextFormTitleNumberState extends State<ComponentsTextFormTitleNumber> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            widget.title,
            style: TextStyle(color: colorGrey, fontSize: fontSizeMedium),
          ),
          Container(
            child: FormBuilderTextField(
              keyboardType: TextInputType.number,
              name: widget.name,
              decoration: InputDecoration(
                hintText: widget.hintText,
                hintStyle: TextStyle(height: 1.0),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                  borderSide: BorderSide(
                      color: colorNormal,
                      width: 2),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                  borderSide: BorderSide(
                      color: colorDark,
                      width: 2),
                ),
              ),
              textInputAction: TextInputAction.next,
              validator: (String? value) {
                if (value == null || value.isEmpty) {
                  return '필수 값 입니다. 다시 확인해 주세요.';
                }
                return null;
              },
            ),
            width: MediaQuery.of(context).size.width / 2.6,
            height: 45,
            margin: widget.margin,
          ),
        ],
      ),
    );
  }
}
