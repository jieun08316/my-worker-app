import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/config/size.dart';

class ComponentTextForm extends StatefulWidget {
  const ComponentTextForm({
    super.key,
    required this.title,
    required this.name,
    required this.obscureText,
    required this.hintText,
    required this.lines,
    required this.maxLength,
  });

  final String title;
  final String name;
  final String hintText;
  final bool obscureText;
  final int lines;
  final int maxLength;

  @override
  State<ComponentTextForm> createState() => _ComponentTextFormState();
}

class _ComponentTextFormState extends State<ComponentTextForm> {
  @override
  Widget build(BuildContext context) {
    return _test();
  }

  Widget _test() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.title,
          style: const TextStyle(
            fontSize: fontSizeMedium,
          ),
        ),
        const SizedBox(height: 5.0,),
        FormBuilderTextField(
          obscureText: widget.obscureText,
          maxLength: widget.maxLength,
          name: widget.name,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            hintText: widget.hintText,
            hintStyle: TextStyle(height: 1.0),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide:
              BorderSide(color: colorDark, width: 2),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide:
              BorderSide(color: colorNormal, width: 2),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide:
              BorderSide(color: Colors.red, width: 2),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide:
              BorderSide(color: Colors.red, width: 2),
            ),
          ),
          textInputAction: TextInputAction.next,
          validator: (String? value) {
            if (value == null || value.isEmpty) {
              return '필수 값 입니다. 다시 확인해 주세요.';
            }
            return null;
          },
        ),
      ],
    );
  }
}
