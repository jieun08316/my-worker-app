import 'package:flutter/material.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';

enum SampleItem { itemOne, itemTwo }

class ComponentsAppbarManualManageDetail extends StatelessWidget implements PreferredSizeWidget {
  const ComponentsAppbarManualManageDetail({super.key});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: BackButton(
        color: Colors.white,
      ),
      title: Text(
          '매뉴얼 관리',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
      centerTitle: true,
      actions: [
        PopupMenuButton(
          child: ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: Image.asset(
                'assets/img/three_dots.png',
                color: Colors.white,
                width: 26,
                height: 26,
              )),
          color: Colors.white,
          onSelected: (value) {
            if (value == "수정하기") {
              // add desired output
            } else if (value == "삭제하기") {
              showDialog<String>(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                  title: const Text('삭제하시겠습니까?', style: TextStyle(color: Color.fromRGBO(64, 64, 64, 1.0)),),
                  // content: const Text('AlertDialog description'),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context, 'Cancel'),
                      child: const Text('취소', style: TextStyle(color: colorNormal),),
                    ),
                    TextButton(
                      onPressed: () => Navigator.pop(context, 'OK'),
                      child: const Text('확인', style: TextStyle(color: colorDark)),
                    ),
                  ],
                  backgroundColor: Colors.white,
                ),
              );
              // add desired output
            }
          },
          itemBuilder: (BuildContext context) =>
          <PopupMenuEntry>[
            PopupMenuItem(
              value: "수정하기",
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Image.asset(
                      'assets/img/pencil.png',
                      color: colorDark,
                      width: 25,
                      height: 25,
                    ),
                  ),
                  const Text(
                    '수정하기',
                    style: TextStyle(fontSize: fontSizeSmall),
                  ),
                ],
              ),
            ),
            PopupMenuItem(
              value: "삭제하기",
              child: Row(
                children: [
                  Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Image.asset(
                        'assets/img/delete.png',
                        color: colorDark,
                        width: 25,
                        height: 25,
                      )),
                  const Text(
                    '삭제하기',
                    style: TextStyle(fontSize: fontSizeSmall),
                  ),
                ],
              ),
            ),
          ],
        )
      ],
      backgroundColor: colorDark,
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(50);

}
