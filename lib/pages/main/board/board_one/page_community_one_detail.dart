import 'package:flutter/material.dart';
import 'package:my_worker_app/components/community/together/detail/components_together_community_detail_comment.dart';
import 'package:my_worker_app/components/components_loading.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/model/home/community/one/one_community_comment.dart';
import 'package:my_worker_app/model/home/community/one/one_community_detail.dart';
import 'package:my_worker_app/pages/main/store_main/work_place/page_index.dart';
import 'package:my_worker_app/repository/home/repo_board.dart';

enum SampleItem { itemOne, itemTwo, itemThree }

class PageCommunityOneDetail extends StatefulWidget {
  const PageCommunityOneDetail({
    super.key,
    required this.id,
  });

  final num id;

  @override
  State<PageCommunityOneDetail> createState() => _PageCommunityOneDetailState();
}

class _PageCommunityOneDetailState extends State<PageCommunityOneDetail> {

  OneCommunityDetail? _detailOne;

  List<OneCommunityComment> _detailComment = [];

  Future<void> _loadDetail() async {
    await RepoBoard().getProductOneContent(widget.id)
        .then((res) => {
      setState(() {
        _detailOne = res.data;
        _detailComment = res.data.comments;
      })
    });
  }

  Future<void> _loadDelete() async {
    await RepoBoard().deleteBoard(widget.id)
        .then((res) => {
    }).catchError((err) {
      debugPrint(err);
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          '을의 이야기',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        actions: [
          PopupMenuButton(
            child: ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: Image.asset(
                  'assets/img/three_dots.png',
                  color: Colors.white,
                  width: 26,
                  height: 26,
                )),
            color: Colors.white,
            onSelected: (value) {
              if (value == "수정하기") {
                // add desired output
              } else if (value == "삭제하기") {
                showDialog<String>(
                  context: context,
                  builder: (BuildContext context) => AlertDialog(
                    title: const Text('삭제하시겠습니까?', style: TextStyle(color: colorGrey),),
                    // content: const Text('AlertDialog description'),
                    actions: <Widget>[
                      TextButton(
                        onPressed: () => Navigator.pop(context, 'Cancel'),
                        child: const Text('취소', style: TextStyle(color: colorNormal),),
                      ),
                      TextButton(
                        onPressed: (){
                          _loadDelete();
                          Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => PageIndex(id: widget.id,))
                          );
                        },
                        child: const Text('확인', style: TextStyle(color: colorDark)),
                      ),
                    ],
                    backgroundColor: Colors.white,
                  ),
                );
                // add desired output
              }
            },
            itemBuilder: (BuildContext context) =>
            <PopupMenuEntry>[
              PopupMenuItem(
                value: "수정하기",
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Image.asset(
                        'assets/img/pencil.png',
                        color: colorDark,
                        width: 25,
                        height: 25,
                      ),
                    ),
                    const Text(
                      '수정하기',
                      style: TextStyle(fontSize: fontSizeSmall),
                    ),
                  ],
                ),
              ),
              PopupMenuItem(
                value: "삭제하기",
                child: Row(
                  children: [
                    Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Image.asset(
                          'assets/img/delete.png',
                          color: colorDark,
                          width: 25,
                          height: 25,
                        )),
                    const Text(
                      '삭제하기',
                      style: TextStyle(fontSize: 15),
                    ),
                  ],
                ),
              ),
            ],
          )
        ],
        backgroundColor: colorNormal,
      ),
      body: SingleChildScrollView(
          child: Column(
            children: [
              _tabMenuOneBoard(),
              _tabMenuTogetherComment()
            ],
          )
      ),
    );
  }

  Widget _tabMenuOneBoard() {
    if (_detailOne == null) {
      return SizedBox(
          child: ComponentsLoading()
      );
    } else {
      return SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        '${_detailOne!.title}',
                        style: TextStyle(
                            color: colorNormal,
                            fontSize: fontSizeMedium
                        ),
                      ),
                      Container(
                        child: Text(
                          '${_detailOne!.dateBoard}',
                          style: TextStyle(
                              color: colorGrey,
                              fontSize: 9
                          ),
                        ),
                        margin: EdgeInsets.fromLTRB(0, 15, 5, 0),
                      ),
                    ],
                  ),
                  margin: EdgeInsets.fromLTRB(10, 0, 0, 5),
                ),
                Container(
                  color: colorLight,
                  height: 1,
                ),
                Container(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Text(
                          '${_detailOne!.boardImgUrl}',
                          style: TextStyle(
                              fontSize: fontSizeSmall
                          ),
                        ),
                      ),
                    ],
                  ),
                  margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                ),
                Container(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Text(
                          '${_detailOne!.content}',
                          style: TextStyle(
                              fontSize: fontSizeSmall
                          ),
                        ),
                      ),
                    ],
                  ),
                  margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                ),
                Container(
                  child: Row(
                    children: [
                      Text('댓글 '),
                      Text('18개', style: TextStyle(color: colorDark, fontWeight: FontWeight.bold),),
                    ],
                  ),
                  margin: EdgeInsets.fromLTRB(10, 20, 0, 0),
                ),
                Divider(
                  color: Colors.grey,
                ),
              ],
            ),
            margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
            color: Color.fromRGBO(245, 248, 255, 1.0),
            width: MediaQuery.of(context).size.width / 1.0,
          )
      );
    }
  }


  Widget _tabMenuTogetherComment() {
    return ListView.builder(
      itemCount: _detailComment.length,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (BuildContext cts, int idx) {
        return ComponentsTogetherCommunityDetailComment(callback: () {}, oneCommunityComment:  _detailComment[idx],);
      },
    );
  }
}
