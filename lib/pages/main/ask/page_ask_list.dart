import 'package:flutter/material.dart';
import 'package:my_worker_app/components/appbar/components_appbar_detail.dart';
import 'package:my_worker_app/components/ask/components_ask_list.dart';
import 'package:my_worker_app/components/components_loading.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/home/ask/ask_list.dart';
import 'package:my_worker_app/repository/home/repo_ask.dart';

import 'page_ask_detail.dart';
import 'page_ask_regist.dart';

class PageAskList extends StatefulWidget {
  const PageAskList({super.key});

  @override
  State<PageAskList> createState() => _PageAskListState();
}

class _PageAskListState extends State<PageAskList> {

  final _scrollController = ScrollController();

  List<AskList> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  @override
  void initState() {
    // super: 부모
    // 부모 먼저 준비
    super.initState();

    // 해당 페이지 죽기 전까지 계속 감시하기
    _scrollController.addListener(() {
      // 스크롤이 바닥 쳤을 때 _loadItems 실행
      if (_scrollController.offset == _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });

    _loadItems(reFresh: true);
  }

  Future<void> _loadItems({bool reFresh = false}) async {
    if (reFresh) {
      setState(() {
        _list = [];
        _currentPage = 1;
        _totalPage = 1;
        _totalItemCount = 0;
      });
    }

    if (_currentPage <= _totalPage) {
      await RepoAsk().getList(page: _currentPage)
          .then((res) {
        setState(() {
          _totalPage = res.totalPage.toInt();
          _totalItemCount = res.totalCount.toInt();
          _list = [..._list,...res.list!];

          _currentPage++;
        });
      }).catchError((err) {
        print('실패임');
        debugPrint(err);
      });
    }

    if (reFresh) {
      _scrollController.animateTo(
          0,
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentsAppbarDetail(title: '고객센터',),

      body: ListView(
        controller: _scrollController,
        children: [_askList(),],
      ),
      floatingActionButton: FloatingActionButton.small(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const PageAskRegist())
          );
        },
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        backgroundColor: colorNormal,
        shape: (RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))),
      ),
    );
  }

  Widget _askList() {
    if (_totalItemCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListView.builder(
            itemCount: _list.length,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext ctx, int idx) {
              return ComponentsAskList(
                callback: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageAskDetail(id: _list[idx].id,)));
                }, askList: _list[idx],);
            },
          ),
        ],
      );
    } else {
      return SizedBox(
          child: ComponentsLoading()
      );
    }
  }
}
