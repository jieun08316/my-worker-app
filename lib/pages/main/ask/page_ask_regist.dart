import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:image_picker/image_picker.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/home/ask/ask_create_request.dart';
import 'package:my_worker_app/repository/home/repo_ask.dart';

import 'page_ask_list.dart';

class PageAskRegist extends StatefulWidget {
  const PageAskRegist({Key? key}) : super(key: key);

  @override
  State<PageAskRegist> createState() => _PageAskRegistState();
}

class _PageAskRegistState extends State<PageAskRegist> {
  final _formKey = GlobalKey<FormBuilderState>();
  final picker = ImagePicker();
  XFile? image; // 카메라로 촬영한 이미지를 저장할 변수
  List<XFile?> multiImage = []; // 갤러리에서 여러 장의 사진을 선택해서 저장할 변수
  List<XFile?> images = []; // 가져온 사진들을 보여주기 위한 변수

  // request 받아서 값 넣어주기
  Future<void> sendAsk(AskCreateRequest request) async {
    // id 값 억지로 넣어준 거임 바꿔주기
    await RepoAsk()
        .setAsk(request)
        // 성공했을시에만 페이지 이동되도록 하기
        .then((value) {
      request.title = '';
      request.content = '';
      request.questionImgUrl = '';
      print("성공");
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => const PageAskList()));
    }).catchError((onError) {
      print("실패");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _regist(context),
    );
  }

  Widget _regist(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: BackButton(
            color: Colors.white,
          ),
          title: Text(
            '고객센터 글 작성',
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
          actions: [
            Container(
              child: Image.asset(
                'assets/img/logo_profile.png',
                width: 50,
                height: 50,
              ),
              margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
            ),
          ],
          backgroundColor: colorNormal,
        ),
        body: Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.all(10),
          child: Column(
            children: [
              FormBuilder(
                key: _formKey,
                child: Column(
                  children: [
                    Container(
                      child: FormBuilderTextField(
                        name: 'title',
                        maxLines: null,
                        maxLength: 30,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: '제목을 입력해 주세요.',
                          counterText: '',
                        ),
                      ),
                      margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                      height: 50,
                    ),
                    Divider(
                      color: colorLight,
                    ),
                    Container(
                      child: FormBuilderTextField(
                        name: 'content',
                        maxLines: null,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: '내용을 입력해 주세요',
                        ),
                      ),
                      margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                      height: 450,
                    ),
                  ],
                ),
              ),
              Row(
                children: [
                  Row(
                    children: [
                      Container(
                          margin: EdgeInsets.all(10),
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            color: colorNormal,
                            borderRadius: BorderRadius.circular(20),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 0.5,
                                  blurRadius: 5)
                            ],
                          ),
                          child: IconButton(
                              onPressed: () async {
                                image = await picker.pickImage(
                                    source: ImageSource.camera);
                                //카메라로 촬영하지 않고 뒤로가기 버튼을 누를 경우, null값이 저장되므로 if문을 통해 null이 아닐 경우에만 images변수로 저장하도록 합니다
                                if (image != null) {
                                  setState(() {
                                    images.add(image);
                                  });
                                }
                              },
                              icon: Icon(
                                Icons.add_a_photo,
                                size: 30,
                                color: Colors.white,
                              ))),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                          margin: EdgeInsets.all(10),
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            color: colorNormal,
                            borderRadius: BorderRadius.circular(20),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 0.5,
                                  blurRadius: 5)
                            ],
                          ),
                          child: IconButton(
                              onPressed: () async {
                                multiImage = await picker.pickMultiImage();
                                setState(() {
                                  //multiImage를 통해 갤러리에서 가지고 온 사진들은 리스트 변수에 저장되므로 addAll()을 사용해서 images와 multiImage 리스트를 합쳐줍니다.
                                  images.addAll(multiImage);
                                });
                              },
                              icon: Icon(
                                Icons.add_photo_alternate_outlined,
                                size: 30,
                                color: Colors.white,
                              ))),
                    ],
                  ),
                ],
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16.0),
                        child: ElevatedButton(
                          onPressed: () {
                            if (_formKey.currentState!.saveAndValidate()) {
                              showDialog<String>(
                                context: context,
                                builder: (BuildContext context) => AlertDialog(
                                  title: const Text(
                                    '등록하시겠습니까?',
                                    style: TextStyle(
                                        color: colorGrey),
                                  ),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () =>
                                          Navigator.pop(context, 'Cancel'),
                                      child: const Text(
                                        '취소',
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                63, 114, 175, 1.0)),
                                      ),
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        AskCreateRequest request =
                                            AskCreateRequest(
                                          _formKey.currentState!
                                              .fields['title']!.value,
                                          _formKey.currentState!
                                              .fields['content']!.value,
                                          _formKey.currentState!
                                              .fields['questionImgUrl']?.value,
                                        );
                                        sendAsk(request);
                                      },
                                      child: const Text('확인',
                                          style: TextStyle(color: colorNormal)),
                                    ),
                                  ],
                                  backgroundColor: Colors.white,
                                ),
                              );
                            }
                          },
                          style: ElevatedButton.styleFrom(
                              primary: Colors.white,
                              onPrimary: colorNormal,
                              side: BorderSide(color: colorNormal, width: 2.0)),
                          child: const Text('등록'),
                        ),
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 60, 0),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: ElevatedButton(
                        onPressed: () {
                          showDialog<String>(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                              title: const Text(
                                '취소하시겠습니까?',
                                style: TextStyle(
                                    color: colorGrey),
                              ),
                              // content: const Text('AlertDialog description'),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () =>
                                      Navigator.pop(context, 'Cancel'),
                                  child: const Text(
                                    '취소',
                                    style: TextStyle(color: colorNormal),
                                  ),
                                ),
                                TextButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                (PageAskList())));
                                  },
                                  child: const Text('확인',
                                      style: TextStyle(
                                          color:
                                              colorDark)),
                                ),
                              ],
                              backgroundColor: Colors.white,
                            ),
                          );
                        },
                        style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            onPrimary: colorNormal,
                            side: BorderSide(color: colorNormal, width: 2.0)),
                        child: const Text('취소'),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
