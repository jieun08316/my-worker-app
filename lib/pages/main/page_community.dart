import 'package:flutter/material.dart';
import 'package:my_worker_app/components/appbar/components_appbar_detail.dart';
import 'package:my_worker_app/components/community/one/components_one_community_list.dart';
import 'package:my_worker_app/components/community/together/components_together_community_list.dart';
import 'package:my_worker_app/components/components_loading.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/home/community/one/one_community_list.dart';
import 'package:my_worker_app/pages/main/board/board_one/page_community_one_detail.dart';
import 'package:my_worker_app/pages/main/board/board_together/page_community_together_detail.dart';
import 'package:my_worker_app/repository/home/repo_board.dart';

class PageCommunity extends StatefulWidget {
  const PageCommunity({super.key});

  @override
  State<PageCommunity> createState() => _PageCommunityState();
}

class _PageCommunityState extends State<PageCommunity>
    with SingleTickerProviderStateMixin {

  final _scrollController = ScrollController();
  final _scrollControllerOne = ScrollController();

  List<OneCommunityList> _listOneBoard = [];
  int _currentPageOne = 1;
  int _totalPageOne = 1;
  int _totalItemCountOne = 0;

  List<OneCommunityList> _listTogetherBoard = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  late TabController tabController = TabController(
    length: 2,
    vsync: this,
    initialIndex: 0,


    /// 탭 변경 애니메이션 시간
    animationDuration: const Duration(milliseconds: 500),
  );

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    // super: 부모
    // 부모 먼저 준비
    super.initState();

    // 해당 페이지 죽기 전까지 계속 감시하기
    _scrollController.addListener(() {
      // 스크롤이 바닥 쳤을 때 _loadItems 실행
      if (_scrollController.offset == _scrollController.position.maxScrollExtent) {
        _loadItemTogether();
      }
    });

    _loadItemTogether(reFresh: true);

    _scrollControllerOne.addListener(() {
      // 스크롤이 바닥 쳤을 때 _loadItems 실행
      if (_scrollControllerOne.offset == _scrollControllerOne.position.maxScrollExtent) {
        _loadItemsOne();
      }
    });

    _loadItemsOne(reFresh: true);
  }

  Future<void> _loadItemTogether({bool reFresh = false}) async {
    if (reFresh) {
      setState(() {
        _listTogetherBoard = [];
        _currentPage = 1;
        _totalPage = 1;
        _totalItemCount = 0;
      });
    }


    if (_currentPage <= _totalPage) {
      await RepoBoard().getList(page: _currentPage)
          .then((res) {
        setState(() {
          _totalPage = res.totalPage.toInt();
          _totalItemCount = res.totalCount.toInt();
          _listTogetherBoard = [..._listTogetherBoard,...res.list!];

          _currentPage++;
        });
      }).catchError((err) {
        debugPrint(err);
      });
    }

    if (reFresh) {
      _scrollController.animateTo(
          0,
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut
      );
    }
  }


  Future<void> _loadItemsOne({bool reFresh = false}) async {
    if (reFresh) {
      setState(() {
        _listOneBoard = [];
        _currentPageOne = 1;
        _totalPageOne = 1;
        _totalItemCountOne = 0;
      });
    }

    if (_currentPageOne <= _totalPageOne) {
      await RepoBoard().getListOne(page: _currentPageOne)
          .then((res) {
        setState(() {
          _totalPageOne = res.totalPage.toInt();
          _totalItemCountOne = res.totalCount.toInt();
          _listOneBoard = [..._listOneBoard,...res.list!];

          _currentPageOne++;
        });
      }).catchError((err) {
        debugPrint(err);
      });
    }

    if (reFresh) {
      _scrollControllerOne.animateTo(
          0,
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentsAppbarDetail(title: '커뮤니티'),
      body: DefaultTabController(
          length: 2,
          child: Column(
            children: [
              TabBar(
                controller: tabController,
                tabs: const [
                  Tab(text: "을의 "
                      "이야기"),
                  Tab(text: "갑과 을의 이야기"),
                ],
                indicatorSize: TabBarIndicatorSize.label,
                indicatorColor: colorNormal,
                indicatorWeight: 2,
                labelColor: colorNormal,
                labelStyle: const TextStyle(
                  fontSize: fontSizeMedium,
                  fontWeight: FontWeight.bold,
                ),
                unselectedLabelColor: Colors.grey,
                unselectedLabelStyle: const TextStyle(
                  fontSize: fontSizeSmall,
                ),
                // overlayColor: MaterialStatePropertyAll(
                //   colorLight,
                // ),
                splashBorderRadius: BorderRadius.circular(20),
              ),
              Expanded(
                child: TabBarView(
                  controller: tabController,
                  children: [
                    ListView(
                      controller: _scrollControllerOne,
                      children: [
                        IconButton(
                          onPressed: () {
                            //Navigator.push(context, MaterialPageRoute(builder: (context) => PageBoardOneRegist()));
                          },
                          icon: Icon(Icons.add, color: colorNormal),
                        ),
                        Divider(color: colorNormal),
                        _tabMenuOneBoard(),
                      ],
                    ),
                    ListView(
                      controller: _scrollController,
                      children: [
                        IconButton(
                          onPressed: () {
                            //Navigator.push(context, MaterialPageRoute(builder: (context) => PageBoardRegist()));
                          },
                          icon: Icon(Icons.add, color: colorNormal,
                            size: 30,),
                        ),
                        Divider(
                            color: colorNormal
                        ),
                        _tabMenuTogetherBoard(),
                      ],
                    ),
                  ],
                ),
              )
            ],
          )
      ),
    );
  }
  Widget _tabMenuTogetherBoard() {
    if (_totalItemCount > 0) {
      return ColoredBox(
        color: colorLight,
        child: Column(
          children: [
            ListView.builder(
              itemCount: _listTogetherBoard.length,
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext ctx, int idx) {
                return  ComponentsTogetherCommunityList(
                  callback: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageCommunityTogetherDetail(id: _listTogetherBoard[idx].id,)));
                  }, togetherCommunityList: _listTogetherBoard[idx],);
              },
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
          child: ComponentsLoading()      );
    }
  }

  Widget _tabMenuOneBoard() {
    if (_totalItemCountOne > 0) {
      return ColoredBox(
        color: colorLight,
        child: Column(
          children: [
            ListView.builder(
              itemCount: _listOneBoard.length,
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext ctx, int idx) {
                return  ComponentsOneCommunityList(
                  callback: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageCommunityOneDetail(id: _listOneBoard[idx].id,)));
                  }, oneCommunityList: _listOneBoard[idx],);
              },
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
          child: ComponentsLoading()
      );
    }
  }


}
