import 'package:flutter/material.dart';
import 'package:my_worker_app/components/main/component_null_item.dart';
import 'package:my_worker_app/components/main/component_work_place_choice.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/main/my_work_place_item.dart';
import 'package:my_worker_app/pages/main/store_main/work_place/page_index.dart';
import 'package:my_worker_app/pages/main/page_community.dart';
import 'package:my_worker_app/pages/main/page_my_page.dart';
import 'package:my_worker_app/repository/repo_info.dart';

// 로그인 후 이동하는 페이지
// 내정보, 커뮤니티, 사업장 선택가능

class PagePartTimeIndex extends StatefulWidget {
  const PagePartTimeIndex({super.key});

  @override
  State<PagePartTimeIndex> createState() => _PagePartTimeIndexState();
}

class _PagePartTimeIndexState extends State<PagePartTimeIndex> {
  //사업장 리스트 모델
  List<MyWorkPlaceItem> _list = [];

  @override
  void initState() {
    super.initState();
    // 사업장 리스트 초기화
    _loadItems();
  }

  // [GET] 회원이 속한 사업장 리스트 가져오기 API
  Future<void> _loadItems() async {
    await RepoInfo().getList().then((res) {
      setState(() {
        _list = res.list!;
      });
    }).catchError((err) {
      // debugPrint(err);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: colorNormal,
        title: const Center(
          child: Text(
            '알바야',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
        ),

        //로고 바로 아래에 들어가니 이 로고는 필요없을듯 추가할 경우 간단한 아이콘으로 해서 처리하는 게 나을 것 같아요
        // actions: [
        // Image.asset(
        //   'assets/img/logo_t.png',
        //   width: 50,
        //   height: 50,
        // )
        // ],
      ),

      /** APPBAR 종료 **/
      /** BODY 시작**/
      body: SingleChildScrollView(
        child: Column(
          children: [
            //로고 이미지
            Container(
              width: MediaQuery.of(context).size.width - 300 / 2,
              margin: const EdgeInsets.fromLTRB(0, 30, 0, 30),
              child: Image.asset('assets/img/logo_profile.png'),
            ),

            // 커뮤니티 & 내정보
            // 버튼 간격 띄어줌
            // 스타일 같게 줌
            Row(
              //정렬 추가
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                /** 커뮤니티 버튼**/
                SizedBox(
                  width: MediaQuery.of(context).size.width / 2 - 20,
                  child: Center(
                    child: ElevatedButton(
                      onPressed: () {
                        // 커뮤니티 페이지로 이동
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => PageCommunity()));
                      },
                      style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          side: const BorderSide(
                            color: colorNormal,
                            width: 2,
                          )),
                      child: Container(
                        width: 80,
                        height: 60,
                        margin: const EdgeInsets.all(10),
                        child: const Column(
                          children: [
                            Icon(
                              Icons.comment,
                              color: colorNormal,
                              size: 35,
                            ),
                            Text(
                              '커뮤니티',
                              style: TextStyle(
                                  color: colorNormal,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                /** 내정보 버튼**/
                SizedBox(
                  width: MediaQuery.of(context).size.width / 2 - 20,
                  child: Center(
                    child: ElevatedButton(
                      onPressed: () {
                        // 내 정보 페이지로 이동
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => PageMyPage()));
                      },
                      style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          side: const BorderSide(
                            color: colorNormal,
                            width: 2,
                          )),
                      child: Container(
                        width: 80,
                        height: 60,
                        margin: const EdgeInsets.all(10),
                        child: const Column(
                          children: [
                            Icon(
                              Icons.account_circle,
                              color: colorNormal,
                              size: 35,
                            ),
                            Text(
                              '마이 페이지',
                              style: TextStyle(
                                  color: colorNormal,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),

            /**사업장 그리드 시작**/
            Container(
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width - 32 / 1.1,
              height: MediaQuery.of(context).size.height / 3 + 20,
              margin: const EdgeInsets.only(top: 20),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: colorLight,
                  border: Border.all(
                    color: colorNormal,
                    width: 2,
                  )),
              child: Column(
                  children: [
                    _list.isNotEmpty
                        ? Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.all(10),
                          child: const Text(
                            '근무지 리스트',
                            style: TextStyle(
                                color: colorNormal,
                                fontSize: 17,
                                fontWeight: FontWeight.bold),
                          ),
                        ),Divider(color: Colors.white,),
                      ],
                    )
                        : Container(
                      margin: const EdgeInsets.all(10),
                      child: const Text(
                        '',
                        style: TextStyle(
                            color: colorNormal,
                            fontSize: 17,
                            fontWeight: FontWeight.bold),
                      ),
                    ),


                    Container(
                      alignment: Alignment.center,
                      padding: const EdgeInsets.only(left:20 ,right:20 ,),
                      margin: EdgeInsets.only(top: 5),
                      // 사업장 리스트가 있으면 리스트 그리드뷰 , 없으면 없다고 안내 컴포넌트
                      child: _list.isNotEmpty
                          ? GridView.builder(
                          itemCount: _list.isNotEmpty ? _list.length : 1,
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3,
                              childAspectRatio: 1,
                              mainAxisSpacing: 1,
                              crossAxisSpacing: 15),
                          itemBuilder: (BuildContext ctx, int idx) {
                            return ComponentWorkPlaceChoice(
                              myWorkPlaceItem: _list[idx],
                              callback: () {
                                Navigator.of(context).push(
                                  // 사업장 아이디 받아와서 페이지 이동
                                  // 안됐던 이유 : PageIndex에 const 처리되어있었음
                                    MaterialPageRoute(
                                        builder: (context) => PageIndex(
                                            id: _list[idx].businessId)));
                              },
                            );
                          })
                          : const ComponentNullItem(),
                      /** 그리드뷰  End**/
                    ),
                  ]),
            ),
          ],
        ),
      ),
    );
  }
}
