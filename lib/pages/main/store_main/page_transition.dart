import 'package:flutter/material.dart';
import 'package:my_worker_app/components/appbar/component_appbar_base.dart';
class PageTransition extends StatefulWidget {
  const PageTransition({super.key});

  @override
  State<PageTransition> createState() => _PageTransitionState();
}

class _PageTransitionState extends State<PageTransition> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarBase(title: '인수인계'),
    body: SingleChildScrollView(child:
      Text('인수인계 페이지입니다.'),),

    );
  }
}
