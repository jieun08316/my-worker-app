import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_worker_app/components/appbar/component_appbar_base.dart';

import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/home/transition/transition_regist_request.dart';
import 'package:my_worker_app/repository/my_page/repo_transition.dart';

import 'page_transition_list.dart';

class PageTransitionRegist extends StatefulWidget {
  const PageTransitionRegist({super.key, required this.businessId});
  final num businessId;
  @override
  State<PageTransitionRegist> createState() => _PageTransitionRegistState();
}

class _PageTransitionRegistState extends State<PageTransitionRegist> {

  final _formKey = GlobalKey<FormBuilderState>();

  // request 받아서 값 넣어주기
  Future<void> sendManual(TransitionRegistRequest request) async {
    await RepoTransition().setTransition(request, widget.businessId)
    // 성공했을시에만 페이지 이동되도록 하기
        .then((value) {
      request.content = '';
      print("등록 성공");
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  PageTransitionList(BusinessId: widget.businessId,)));
    }).catchError((onError) {
      print("등록 실패");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _regist(context),
    );
  }

  Widget _regist(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarBase(title: '인수인계 등록',),
      body: SingleChildScrollView(
        child: Column(
          children: [
            FormBuilder(
              key: _formKey,
              child: Container(
                    child: FormBuilderTextField(
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(RegExp(r'[0-9a-zA-Zㄱ-ㅎ가-힣]')),
                      ],
                      keyboardType: TextInputType.text,
                      name: 'content',
                      maxLines: null,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: '내용을 입력해 주세요',
                      ),
                    ),
                    margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                    height: 450,
                  ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState!.saveAndValidate()) {
                            showDialog<String>(
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                title: const Text(
                                  '등록하시겠습니까?',
                                  style: TextStyle(
                                      color: colorGrey),
                                ),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.pop(context, 'Cancel'),
                                    child: const Text(
                                      '취소',
                                      style: TextStyle(
                                          color:
                                         colorDark),
                                    ),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      TransitionRegistRequest request =
                                      TransitionRegistRequest(
                                        _formKey.currentState!.fields['content']!.value,
                                      );
                                      sendManual(request);
                                    },
                                    child: const Text('확인',
                                        style: TextStyle(
                                            color:
                                            colorNormal)),
                                  ),
                                ],
                                backgroundColor: Colors.white,
                              ),
                            );
                          }
                        },
                        style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            onPrimary: colorNormal,
                            side: BorderSide(
                                color: colorNormal,
                                width: 2.0)),
                        child: const Text('등록'),
                      ),
                    ),
                    margin: EdgeInsets.fromLTRB(0, 0, 60, 0),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: ElevatedButton(
                      onPressed: () {
                        showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            title: const Text(
                              '취소하시겠습니까?',
                              style: TextStyle(
                                  color: colorGrey),
                            ),
                            // content: const Text('AlertDialog description'),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () => Navigator.pop(context, 'Cancel'),
                                child: const Text(
                                  '취소',
                                  style: TextStyle(color: colorNormal),
                                ),
                              ),
                              TextButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => (PageTransitionList(BusinessId: widget.businessId,))));
                                },
                                child: const Text('확인',
                                    style: TextStyle(
                                        color: colorNormal)),
                              ),
                            ],
                            backgroundColor: Colors.white,
                          ),
                        );
                      },
                      style: ElevatedButton.styleFrom(
                          primary: Colors.white,
                          onPrimary: colorNormal,
                          side: BorderSide(color: colorNormal, width: 2.0)),
                      child: const Text('취소'),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
