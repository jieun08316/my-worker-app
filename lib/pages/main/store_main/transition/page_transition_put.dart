import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_worker_app/components/appbar/component_appbar_base.dart';

import 'package:my_worker_app/components/components_loading.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/home/transition/transition_detail.dart';
import 'package:my_worker_app/model/home/transition/transition_put_request.dart';
import 'package:my_worker_app/repository/my_page/repo_transition.dart';

import 'page_transition_list.dart';
class PageTransitionPut extends StatefulWidget {
  const PageTransitionPut({super.key, required this.id});

  final num id;

  @override
  State<PageTransitionPut> createState() => _PageTransitionPutState();
}

class _PageTransitionPutState extends State<PageTransitionPut> {

  final _formKey = GlobalKey<FormBuilderState>();
  TransitionDetail? _putDetail;

  @override
  void initState() {
    super.initState();

    _loadDetail();
  }

  Future<void> _loadDetail() async {
    await RepoTransition().getTransition(widget.id)
        .then((res) => {
      setState(() {
        _putDetail = res.data;
      })
    });
  }

  Future<void> putTransition (TransitionPutRequest request) async {
    await RepoTransition().putTransition(request, widget.id)
        .then((value) {
      request.content = '';
      print("성공");
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => PageTransitionList(BusinessId: widget.id,)));
    }).catchError((onError) {
      print(onError);
      print("실패");
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_putDetail != null) {
      return Scaffold(
        body: _regist(context),
      );
    } else {
      return ComponentsLoading();
    }
  }

  Widget _regist(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarBase(title: '인수인계 수정',),
      body: SingleChildScrollView(
        child: Column(
          children: [
            FormBuilder(
              key: _formKey,
              child: Container(
                child: FormBuilderTextField(
                  initialValue: '${_putDetail!.content}',
                  name: 'content',
                  maxLines: null,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: '인수인계를 작성해주세요.',
                  ),
                ),
                margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                height: 450,
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState!.saveAndValidate()) {
                            showDialog<String>(
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                title: const Text(
                                  '등록하시겠습니까?',
                                  style: TextStyle(
                                      color: colorGrey),
                                ),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.pop(context, 'Cancel'),
                                    child: const Text(
                                      '취소',
                                      style: TextStyle(
                                          color:
                                          colorNormal),
                                    ),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      TransitionPutRequest request =
                                      TransitionPutRequest(
                                        _formKey.currentState!.fields['content']!.value,
                                      );
                                      putTransition(request);
                                    },
                                    child: const Text('확인',
                                        style: TextStyle(
                                            color:
                                            colorDark)),
                                  ),
                                ],
                                backgroundColor: Colors.white,
                              ),
                            );
                          }
                        },
                        style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            onPrimary: colorDark,
                            side: BorderSide(
                                color: colorDark,
                                width: 2.0)),
                        child: const Text('등록'),
                      ),
                    ),
                    margin: EdgeInsets.fromLTRB(0, 0, 60, 0),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: ElevatedButton(
                      onPressed: () {
                        showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            title: const Text(
                              '취소하시겠습니까?',
                              style: TextStyle(
                                  color: colorGrey),
                            ),
                            // content: const Text('AlertDialog description'),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () => Navigator.pop(context, 'Cancel'),
                                child: const Text(
                                  '취소',
                                  style: TextStyle(color: colorNormal),
                                ),
                              ),
                              TextButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => (PageTransitionList(BusinessId: widget.id,))));
                                },
                                child: const Text('확인',
                                    style: TextStyle(
                                        color: colorDark)),
                              ),
                            ],
                            backgroundColor: Colors.white,
                          ),
                        );
                      },
                      style: ElevatedButton.styleFrom(
                          primary: Colors.white,
                          onPrimary: colorNormal,
                          side: BorderSide(color: colorNormal, width: 2.0)),
                      child: const Text('취소'),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

}
