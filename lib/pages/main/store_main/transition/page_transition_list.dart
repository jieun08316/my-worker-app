import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:my_worker_app/components/appbar/component_appbar_base.dart';
import 'package:my_worker_app/components/transition/components_transition_list.dart';
import 'package:my_worker_app/model/home/transition/transition_list.dart';
import 'package:my_worker_app/model/home/transition/transition_year_month.dart';
import 'package:my_worker_app/repository/my_page/repo_transition.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';

import 'page_transition_regist.dart';

class PageTransitionList extends StatefulWidget {
  const PageTransitionList({super.key, required this.BusinessId});
  final num BusinessId;
  @override
  State<PageTransitionList> createState() => _PageTransitionListState();
}

class _PageTransitionListState extends State<PageTransitionList> {
  List<TransitionList> _detail = [];

  List<TransitionYearMonth> _month = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _selectedDay = _focusedDay;
    _selectedEvents = ValueNotifier(_getEventsForDay(_selectedDay!));

    _loadItems();
  }

  String month = DateFormat('yyyy').format(DateTime.now());
  String year = DateFormat('MM').format(DateTime.now());

  Future<void> _loadItems() async {
    await RepoTransition()
        .getList(int.parse(month), int.parse(year))
        .then((res) {
      setState(() {
        _detail = res.list!;
        print('OK');
      });
    }).catchError((err) {
      debugPrint(err);
      print('NO');
    });
  }

  // 날짜 원하는 형식으로 바꿈
  String date = DateFormat('yyyy-MM-dd').format(DateTime.now()).toString();

  Future<void> _loadDetail() async {
    // 선택한 날짜를 받아야됨 => 셀렉트데이트가 필요한가?
    await RepoTransition()
        .getDetail(DateFormat('yyyy-MM-dd').format(_selectedDay!))
        .then((res) {
      setState(() {
        _detail = res.list!;
        addDialog(context);
        print('OK');
      });
    }).catchError((err) {
      debugPrint(err);
      print('NO');
    });
  }

  DateTime selectedDate = DateTime.utc(
    DateTime.now().year,
    DateTime.now().month,
    DateTime.now().day,
  );

  late final ValueNotifier<List> _selectedEvents;
  CalendarFormat _calendarFormat = CalendarFormat.month;
  DateTime _focusedDay = DateTime.now();
  DateTime? _selectedDay;
  int selectMarker = 0;

  Map<DateTime, List<Event>> events = {
    DateTime.utc(2024, 5, 1): [
      Event('title'),
    ],
    DateTime.utc(2024, 5, 2): [Event('title2')],
    DateTime.utc(2024, 5, 3): [Event('title3')],
    DateTime.utc(2024, 5, 4): [
      Event('title'),
    ],
    DateTime.utc(2024, 5, 5): [Event('title2')],
    DateTime.utc(2024, 5, 6): [Event('title3')],
  };

  List<Event> _getEventsForDay(DateTime day) {
    return events[day] ?? [];
  }

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: ComponentAppbarBase(title: '인수인계 관리'),


      body: Builder(
        builder: (context) => Center(
          child: TableCalendar(
            firstDay: DateTime.utc(2001, 07, 23),
            lastDay: DateTime.utc(2030, 09, 09),
            focusedDay: DateTime.now(),
            headerStyle: const HeaderStyle(
              titleCentered: true,
              titleTextStyle:
                  TextStyle(color: colorDark, fontSize: fontSizeMedium),
              formatButtonVisible: false,
            ),
            calendarStyle: CalendarStyle(
              todayDecoration: BoxDecoration(
                  color: colorNormal, borderRadius: BorderRadius.circular(20)),
              selectedDecoration: BoxDecoration(
                  color: colorLight, borderRadius: BorderRadius.circular(20)),
              markerSize: 10,
              markerDecoration:
                  BoxDecoration(color: colorNormal, shape: BoxShape.circle),
            ),
            calendarBuilders: CalendarBuilders(
              dowBuilder: (context, day) {
                switch (day.weekday) {
                  case 1:
                    return Center(
                      child: Text('월'),
                    );
                  case 2:
                    return Center(
                      child: Text('화'),
                    );
                  case 3:
                    return Center(
                      child: Text('수'),
                    );
                  case 4:
                    return Center(
                      child: Text('목'),
                    );
                  case 5:
                    return Center(
                      child: Text('금'),
                    );
                  case 6:
                    return Center(
                      child: Text('토'),
                    );
                  case 7:
                    return Center(
                      child: Text(
                        '일',
                        style: TextStyle(color: Colors.red),
                      ),
                    );
                }
              },
            ),
            selectedDayPredicate: (day) {
              return isSameDay(_selectedDay, day);
            },
            onDaySelected: (selectedDay, focusedDay) {
              if (!isSameDay(_selectedDay, selectedDay)) {
                setState(() {
                  _selectedDay = selectedDay;
                  _focusedDay = focusedDay;
                });
                _selectedEvents.value = _getEventsForDay(selectedDay);
                // 날짜 선택시 보이는 거 여기 추가
                _loadDetail();
              }
            },
            onFormatChanged: (format) {
              if (_calendarFormat != format) {
                setState(() {
                  _calendarFormat = format;
                });
              }
            },
            onPageChanged: (focusedDay) {
              _focusedDay = focusedDay;
            },
            eventLoader: _getEventsForDay,
          ),
        ),
      ),
    );
  }

  void addDialog(BuildContext context) {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return SingleChildScrollView(
          child: Column(
            children: [
              Container(
                color: Color(0xFF737373),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                    ),
                  ),
                  child: ListView.builder(
                    itemCount: _detail.length,
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext ctx, int idx) {
                      return ComponentsTransitionList(
                        transitionList: _detail[idx],
                      );
                    },
                  ),
                ),
              ),
              _registButton(),
            ],
          ),
        );
      },
    );
  }

  Widget _registButton() {
    return GestureDetector(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => (PageTransitionRegist(businessId: widget.BusinessId,))));
      },
      child: Container(
        child: Icon(
          Icons.add,
          color: colorNormal,
        ),
        height: 40,
      ),
    );
  }
}

class Event {
  String title;

  Event(this.title);
}
