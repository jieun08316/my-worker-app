import 'package:flutter/material.dart';
import 'package:my_worker_app/components/contents/component_quick_item.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/home/home_work_place_info_item.dart';

import 'package:my_worker_app/components/appbar/component_appbar_base.dart';
import 'package:my_worker_app/pages/main/page_community.dart';
import 'package:my_worker_app/repository/home/repo_home.dart';

class PageHome extends StatefulWidget {
  const PageHome({
    super.key,
    required this.id,
  });

  final num id;

  @override
  State<PageHome> createState() => _PageHomeState();
}

// 출퇴근 내용 전부 삭제 => 근무지 정보로 교체함

class _PageHomeState extends State<PageHome> {
  HomeWorkPlaceInfoItem? _infoItem;

  @override
  void initState() {
    super.initState();
    setState(() {
      _loadDetail();
    });
  }

  // [GET] 사업장 상세보기 API 연동
  // 이 코드랑 위에 _infoItem은 다른 페이지에서도 계속 들고다녀야 함
  // 알바생은 1:N 구조이기 때문
  Future<void> _loadDetail() async {
    await RepoHome().getItem(widget.id).then((res) {
      setState(() {
        _infoItem = res.data;
      });
    }).catchError((err) {
      debugPrint(err);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //앱바
      appBar: const ComponentAppbarBase(
        title: '알바야',
      ),
      //바디 부분 buildBody로 위젯처리함
      body: SingleChildScrollView(
        child: _buildBody(context),
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    // double screenWidth = MediaQuery.of(context).size.width - 320 / 2.0;

    // final quickMenu = ['재고관리', '급여내역서', '근로계약서', '근무정보', '추가 데이터', '추가 데이터'];

    //상세 정보가 로딩이 되지 않음을 확인 하는 부분
    //_infoItem(사업장 정보)가  null인지 확인
    if (_infoItem == null) {
      //만약 null인 경우 로딩 중임을 알리는 데이터 없음 안내
      return Center(
          child: Container(
        child: Text("데이터가 없습니다."),
      ));
    } else {
      // 데이터 있을 경우 하기 내용 반환
      return Container(
        child: Column(
          children: [
            // 사업장 정보 노출
            // 주소의 경우 길어질 가능성이 많아서 오른쪽 여백 더 남겨둠
            // 현재 데이터베이스에 목업으로 되어있어서 짧음
            Container(
              height: MediaQuery.of(context).size.height / 5 + 20,
              padding: EdgeInsets.all(10),
              child: ElevatedButton(
                onPressed: () {
                  // 이동하고 싶은 페이지 있으면 추가
                  // ex) 근무지 정보 전부 보기
                },
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    side: const BorderSide(
                      color: colorNormal,
                      width: 1,
                    )),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    //사업장 아이콘
                    Container(
                      child: const Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          // 아이콘
                          Icon(
                            Icons.business_sharp,
                            color: colorNormal,
                            size: 70,
                          ),
                        ],
                      ),
                    ),

                    // 사업장 정보 문구
                    Container(
                      margin: EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            _infoItem!.businessName,
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          Text('대표자명 ${_infoItem!.ownerName}',
                              style: TextStyle(fontSize: 13)),
                          Text('업종 ${_infoItem!.businessType}',
                              style: TextStyle(fontSize: 13)),
                          Text('전화번호 ${_infoItem!.businessPhoneNumber}',
                              style: TextStyle(fontSize: 13)),
                          Text('주소 ${_infoItem!.reallyLocation}',
                              style: TextStyle(fontSize: 13)),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),

            /** 2번 컨테이너 **/
            Container(
              height: 120,
              child: Column(
                children: [
                  Text('${_infoItem!.businessName}',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 23,color: colorDark),),
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.all(10),
                    child: ElevatedButton(
                      onPressed: () {
                        setState(() {});
                      },
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.resolveWith(
                            (Set<MaterialState> states) {
                          if (states.contains(MaterialState.pressed)) {
                            return colorNormal;
                          }
                        }),
                        foregroundColor: MaterialStateProperty.resolveWith(
                            (Set<MaterialState> states) {
                          if (states.contains(MaterialState.pressed)) {
                            return colorLight;
                          }
                        }),
                      ),
                      child: Container(
                        padding: EdgeInsets.all(10),
                        width: MediaQuery.of(context).size.width / 4.5,
                        child: Container(
                            child: Text(
                          '출근하기',
                          style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold, color: colorNormal),
                        )),
                      ),
                    ),
                  )
                ],
              ),
            ),

            //퀵메뉴
            Container(
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width / 1.1,
              margin: const EdgeInsets.only(top: 20),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: colorLight,
                  border: Border.all(
                    color: colorNormal,
                    width: 2,
                  )),
              child: Column(children: [
                Container(
                  margin: const EdgeInsets.all(10),
                  child: const Text(
                    '바로가기',
                    style: TextStyle(
                        color: colorNormal,
                        fontSize: 17,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Divider(color: colorNormal),
                Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width - 32 / 1.1,
                  height: MediaQuery.of(context).size.height / 3 ,
                  margin: EdgeInsets.only(left: 20, right: 20),
                  child: GridView(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        childAspectRatio: 0.9,
                        mainAxisSpacing: 20,
                        crossAxisSpacing: 20),
                    children: [
                      /**첫번째 버튼 퀵메뉴 시작**/
                      ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PageCommunity()));
                        },
                        style: ElevatedButton.styleFrom(
                          shape: CircleBorder(),
                          primary: colorNormal,
                          onPrimary: colorDark,
                        ),
                        child: Column(
                          children: [
                            /**버튼 내용 수정**/
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              width: 80,
                              height: 80,
                              child: Column(
                                children: [
                                  Image.asset(
                                    'assets/img/salary.png',
                                    color: Colors.white,
                                  ),
                                  Text('급여내역',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold))
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      /**2번 버튼 퀵메뉴 시작**/
                      ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PageCommunity()));
                        },
                        style: ElevatedButton.styleFrom(
                          shape: CircleBorder(),
                          primary: colorNormal,
                          onPrimary: colorDark,
                        ),
                        child: Column(
                          children: [
                            /**버튼 내용 수정**/
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              width: 80,
                              height: 80,
                              child: Column(
                                children: [
                                  Image.asset(
                                    'assets/img/stock.png',
                                    color: Colors.white,
                                  ),
                                  Text('재고관리',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold))
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      /**3번 버튼 퀵메뉴 시작**/
                      ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PageCommunity()));
                        },
                        style: ElevatedButton.styleFrom(
                          shape: CircleBorder(),
                          primary: colorNormal,
                          onPrimary: colorDark,
                        ),
                        child: Column(
                          children: [
                            /**버튼 내용 수정**/
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              width: 80,
                              height: 80,
                              child: Column(
                                children: [
                                  Image.asset(
                                    'assets/img/contract.png',
                                    color: Colors.white,
                                  ),
                                  Text('계약서',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold))
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      /**4번 버튼 퀵메뉴 시작**/
                      ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PageCommunity()));
                        },
                        style: ElevatedButton.styleFrom(
                          shape: CircleBorder(),
                          primary: colorNormal,
                          onPrimary: colorDark,
                        ),
                        child: Column(
                          children: [
                            /**버튼 내용 수정**/
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              width: 80,
                              height: 80,
                              child: Column(
                                children: [
                                  Image.asset(
                                    'assets/img/stock.png',
                                    color: Colors.white,
                                  ),
                                  Text('퀵메뉴1',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold))
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      /**5번 버튼 퀵메뉴 시작**/
                      ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PageCommunity()));
                        },
                        style: ElevatedButton.styleFrom(
                          shape: CircleBorder(),
                          primary: colorNormal,
                          onPrimary: colorDark,
                        ),
                        child: Column(
                          children: [
                            /**버튼 내용 수정**/
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              width: 80,
                              height: 80,
                              child: Column(
                                children: [
                                  Image.asset(
                                    'assets/img/stock.png',
                                    color: Colors.white,
                                  ),
                                  Text('퀵메뉴2',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold))
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      /**6번 버튼 퀵메뉴 시작**/
                      ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PageCommunity()));
                        },
                        style: ElevatedButton.styleFrom(
                          shape: CircleBorder(),
                          primary: colorNormal,
                          onPrimary: colorDark,
                        ),
                        child: Column(
                          children: [
                            /**버튼 내용 수정**/
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              width: 80,
                              height: 80,
                              child: Column(
                                children: [
                                  Image.asset(
                                    'assets/img/exit.png',
                                    color: Colors.white,
                                  ),
                                  Text('나가기',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold))
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ]),
            ),
          ],
        ),
      );
    }
  }
}
