import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_worker_app/components/button/components_camera.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/home/manual/manual_regist_request.dart';
import 'package:my_worker_app/repository/my_page/repo_manual.dart';

import 'page_manual_list.dart';


class PageManualRegist extends StatefulWidget {
  const PageManualRegist({Key? key}) : super(key: key);

  @override
  State<PageManualRegist> createState() => _PageManualRegistState();
}

class _PageManualRegistState extends State<PageManualRegist> {
  final _formKey = GlobalKey<FormBuilderState>();

  // request 받아서 값 넣어주기
  Future<void> sendManual(ManualRegistRequest request) async {
    await RepoManual().setManual(request)
    // 성공했을시에만 페이지 이동되도록 하기
        .then((value) {
      request.title = '';
      request.content = '';
      request.manualImgUrl = '';
      print("성공");
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
              const PageManualList()));
    }).catchError((onError) {
      print(onError);
      print("실패");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _regist(context),
    );
  }

  Widget _regist(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          '매뉴얼 작성',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        actions: [
          Container(
            child: Image.asset(
              'assets/img/logo_profile.png',
              width: 50,
              height: 50,
            ),
            margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
          ),
        ],
        backgroundColor:colorNormal,
      ),
      body: Column(
        children: [
          FormBuilder(
            key: _formKey,
            child: Column(
              children: [
                Container(
                  child: FormBuilderTextField(
                    name: 'title',
                    maxLines: null,
                    maxLength: 30,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: '제목을 입력해 주세요.',
                      counterText: '',
                    ),
                  ),
                  margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                  height: 50,
                ),
                Divider(
                  color: colorLight,
                ),
                Container(
                  child: FormBuilderTextField(
                    name: 'content',
                    maxLines: null,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: '내용을 입력해 주세요',
                    ),
                  ),
                  margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                  height: 450,
                ),
                ComponentsCamera(name: 'manualImgUrl',),
              ],
            ),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: ElevatedButton(
                      onPressed: () {
                        if (_formKey.currentState!.saveAndValidate()) {
                          showDialog<String>(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                              title: const Text(
                                '등록하시겠습니까?',
                                style: TextStyle(
                                    color: Color.fromRGBO(64, 64, 64, 1.0)),
                              ),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () =>
                                      Navigator.pop(context, 'Cancel'),
                                  child: const Text(
                                    '취소',
                                    style: TextStyle(
                                        color:
                                        Color.fromRGBO(63, 114, 175, 1.0)),
                                  ),
                                ),
                                TextButton(
                                  onPressed: () {
                                    ManualRegistRequest request =
                                    ManualRegistRequest(
                                      _formKey.currentState!.fields['title']!.value,
                                      _formKey.currentState!.fields['content']!.value,
                                      _formKey.currentState!.fields['manualImgUrl']?.value.toString(),
                                    );
                                    sendManual(request);
                                  },
                                  child: const Text('확인',
                                      style: TextStyle(
                                          color:
                                          Color.fromRGBO(17, 45, 78, 1.0))),
                                ),
                              ],
                              backgroundColor: Colors.white,
                            ),
                          );
                        }
                      },
                      style: ElevatedButton.styleFrom(
                          primary: Colors.white,
                          onPrimary: Color.fromRGBO(17, 45, 78, 1.0),
                          side: BorderSide(
                              color: Color.fromRGBO(17, 45, 78, 1.0),
                              width: 2.0)),
                      child: const Text('등록'),
                    ),
                  ),
                  margin: EdgeInsets.fromLTRB(0, 0, 60, 0),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: ElevatedButton(
                    onPressed: () {
                      showDialog<String>(
                        context: context,
                        builder: (BuildContext context) => AlertDialog(
                          title: const Text(
                            '취소하시겠습니까?',
                            style: TextStyle(
                                color: Color.fromRGBO(64, 64, 64, 1.0)),
                          ),
                          // content: const Text('AlertDialog description'),
                          actions: <Widget>[
                            TextButton(
                              onPressed: () => Navigator.pop(context, 'Cancel'),
                              child: const Text(
                                '취소',
                                style: TextStyle(color: colorNormal),
                              ),
                            ),
                            TextButton(
                              onPressed: () {},
                              child: const Text('확인',
                                  style: TextStyle(
                                      color: Color.fromRGBO(17, 45, 78, 1.0))),
                            ),
                          ],
                          backgroundColor: Colors.white,
                        ),
                      );
                    },
                    style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        onPrimary: colorNormal,
                        side: BorderSide(color: colorNormal, width: 2.0)),
                    child: const Text('취소'),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
