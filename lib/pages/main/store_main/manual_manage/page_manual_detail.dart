import 'package:flutter/material.dart';
import 'package:my_worker_app/components/components_loading.dart';
import 'package:my_worker_app/components/manual_manage/components_appbar_manual_manage_detail.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/home/manual/manual_detail.dart';
import 'package:my_worker_app/repository/my_page/repo_manual.dart';

class PageManualDetail extends StatefulWidget {
  const PageManualDetail({
    super.key,
    required this.id,
  });

  final num id;

  @override
  State<PageManualDetail> createState() => _PageManualDetailState();
}

class _PageManualDetailState extends State<PageManualDetail> {

  ManualDetail? _detail;

  Future<void> _loadDetail() async {
    await RepoManual().getProduct(widget.id)
        .then((res) => {
      setState(() {
        _detail = res.data;
      })
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentsAppbarManualManageDetail(),
      body: SingleChildScrollView(
        child: _ManualDetail(context)
      )
    );
  }

  Widget _ManualDetail(BuildContext context) {
    if (_detail == null) {
      return SizedBox(
        child: ComponentsLoading()
      );
    } else {
      return SingleChildScrollView(
        child: Center(
            child: Column(
              children: [
                Container(
                  child: Row(
                    children: [
                      Container(
                        child: Text(
                          '${_detail!.title}',
                          style: TextStyle(fontSize: fontSizeLarge,),
                        ),
                      ),
                    ],
                  ),
                  height: 30,
                  margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                ),
                Divider(
                  color: colorLight,
                ),
                Column(
                  children: [
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Row(
                              children: [
                                Text(
                                  '작성자: ',
                                  style: TextStyle(
                                    color: Color.fromRGBO(40, 40, 40, 1.0),
                                    fontSize: fontSizeSmall,
                                  ),
                                ),
                                Text(
                                  '${_detail!.memberName}',
                                  style: TextStyle(
                                    color: Color.fromRGBO(40, 40, 40, 1.0),
                                    fontSize: fontSizeSmall,
                                  ),
                                ),
                              ],
                            ),
                            margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                          ),
                          Container(
                            child: Text(
                              '${_detail!.dateManual}',
                              style: TextStyle(
                                color: Color.fromRGBO(40, 40, 40, 1.0),
                                fontSize: fontSizeSmall,
                              ),
                            ),
                            margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                          ),
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                    ),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('${_detail!.content}')
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                    ),
                    Container(
                      child: _imgNull(),
                      width: 150,
                      height: 150,
                    ),
                  ],
                ),
              ],
            ),
          )
      );
    }
  }

  Widget _imgNull () {
    if (_detail?.manualImgUrl == 'null') {
      return SizedBox();
    } else {
      return Text('${_detail?.manualImgUrl}');
    }
  }

}
