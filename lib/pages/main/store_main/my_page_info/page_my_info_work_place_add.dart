// import 'package:flutter/material.dart';
// import 'package:my_worker_app/components/appbar/component_appbar_base.dart';
// import 'package:my_worker_app/config/color.dart';
//
// class PageMyInfoWorkPlace extends StatelessWidget {
//   const PageMyInfoWorkPlace({super.key});
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: ComponentAppbarBase(title: '근무지 등록'),
//       body: SingleChildScrollView(
//         child: Container(
//           child: Column(
//             children: [
//               //조회 칸
//               Column(
//                 children: [
//                   Container(
//                     padding: EdgeInsets.all(20),
//                     child: Text(
//                       '근무지 조회',
//                       style: TextStyle(
//                           fontWeight: FontWeight.w500,
//                           fontSize: 20,
//                           color: Color.fromRGBO(63, 114, 175, 1)),
//                     ),
//                   ),
//                   Container(
//                     width: MediaQuery.of(context).size.width,
//                     child: TextField(
//                       decoration: InputDecoration(
//                           focusedBorder: OutlineInputBorder(
//                               borderRadius: BorderRadius.circular(50), //모서리 둥글게
//                               borderSide: BorderSide(
//                                 color: colorNormal,
//                                 width: 2.0,
//                               )),
//                           enabledBorder: OutlineInputBorder(
//                               borderRadius: BorderRadius.circular(50), //모서리 둥글게
//                               borderSide: BorderSide(
//                                 color: colorNormal,
//                                 width: 2.0,
//                               )),
//                           labelText: '사업자번호를 입력해주세요.',
//                           labelStyle: TextStyle(
//                               color: Color.fromRGBO(190, 190, 190, 1),),),
//                     ),
//                   ),
//                   Container(
//                     padding: EdgeInsets.all(20),
//                     child: ElevatedButton(
//                       onPressed: () {},
//                       style: ButtonStyle(
//                         backgroundColor: MaterialStateProperty.resolveWith(
//                           (Set<MaterialState> states) {
//                             if (states.contains(MaterialState.pressed)) {
//                               return Color.fromRGBO(63, 114, 175, 1);
//                             }
//                           },
//                         ),
//                         foregroundColor: MaterialStateProperty.resolveWith(
//                           (Set<MaterialState> states) {
//                             if (states.contains(MaterialState.pressed)) {
//                               return Color.fromRGBO(17, 45, 78, 1);
//                             }
//                           },
//                         ),
//                       ),
//                       child: Text(
//                         ' 검색하기',
//                       ),
//                     ),
//                   ),
//                 ],
//               ),
//               //검색결과 칸
//               SizedBox(
//                 width: MediaQuery.of(context).size.width,
//                 height: MediaQuery.of(context).size.height / 1.1,
//                 child: ListView.builder(
//                   key: const PageStorageKey("LIST_VIEW"),
//                   itemCount: 1,
//                   itemBuilder: (context, index) {
//                     return Container(
//                       padding: const EdgeInsets.symmetric(vertical: 50),
//                       width: MediaQuery.of(context).size.width,
//                       child: Center(
//                         child: Text(
//                           '검색결과가 없습니다.',
//                           style: TextStyle(
//                               fontSize: 16,
//                               color: colorNormal,
//                               fontWeight: FontWeight.bold),
//                         ),
//                       ),
//                     );
//                   },
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
