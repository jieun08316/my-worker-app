import 'package:flutter/material.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/model/member/member_response.dart';
import 'package:my_worker_app/repository/repo_member.dart';
class PageMyInfoEdit extends StatefulWidget {
  const PageMyInfoEdit({super.key});

  @override
  State<PageMyInfoEdit> createState() => _PageMyInfoEditState();
}

class _PageMyInfoEditState extends State<PageMyInfoEdit> {
  @override
  MemberResponse? _detail;

  Future<void> _loadMemberDetail() async {
    await RepoMember().getMemberDetail()
        .then((res) => {
      setState(() {
        _detail = res.data;
      })
    })
        .catchError((err) =>
    {
      debugPrint(err)
    });
  }

  @override
  void initState() {
    super.initState();
    _loadMemberDetail();
  }

  @override
  Widget build(BuildContext context) {



    return Scaffold(
      //앱바
      appBar: AppBar(
        title:
        const Text(
          '내 정보 관리',
          style: TextStyle(
            fontSize: fontSizeXlarge,
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: const Color.fromRGBO(0, 0, 0, 0),

      ),
      body: _buildBody(context),
    );
  }
  Widget _buildBody(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double paddingValue = (screenWidth - 300.0) / 2.0; // 컨텐츠 내용 width 300으로 맞춤

    if(_detail == null) {
      return const Center(child: CircularProgressIndicator());
    } else {
      return Padding(
        padding: EdgeInsets.symmetric(horizontal: paddingValue),
        child: Column(
          children: [
            Container(
              alignment: Alignment.topLeft,
              child: const Text(
                '내 정보',
                style: TextStyle(
                  fontSize: fontSizeLarge,
                ),
              ),
            ),
            const SizedBox(height: mediumGap,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                // 이름 정보
                Container(
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.topLeft,
                        child: const Text(
                          '이름',
                          style: TextStyle(
                            fontSize: fontSizeSmall,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            _detail!.name,
                            style: const TextStyle(
                                fontSize: fontSizeLarge,
                                color: Color.fromRGBO(66, 126, 245, 1)
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: smallGap,),
                      const Divider(thickness: 1,)
                    ],
                  ),
                ),

                // ID 정보
                Container(
                  child: Column(
                    children: [
                      const SizedBox(height: smallGap,),
                      Container(
                        alignment: Alignment.topLeft,
                        child: const Text(
                          'ID',
                          style: TextStyle(
                            fontSize: fontSizeSmall,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            _detail!.username,
                            style: TextStyle(
                                fontSize: fontSizeLarge,
                                color: Color.fromRGBO(66, 126, 245, 1)
                            ),),
                        ],
                      ),
                      const SizedBox(height: smallGap,),
                      const Divider(thickness: 1,),

                    ],
                  ),
                ),

                // 닉네임 정보
                Container(
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.topLeft,
                        child: const Text(
                          '닉네임',
                          style: TextStyle(
                            fontSize: fontSizeSmall,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            _detail!.nickName, //모델로 받아와서 이름 넣어주기
                            style: TextStyle(
                                fontSize: fontSizeLarge,
                                color: Color.fromRGBO(66, 126, 245, 1)
                            ),),
                          SizedBox(
                            height: 35,
                            width: 75,
                            child: OutlinedButton(
                              onPressed: (){},
                              style: OutlinedButton.styleFrom(
                                  side: const BorderSide(color: Color.fromRGBO(158, 158, 158, 1))
                              ),
                              child: const Text(
                                '수정',
                                style: TextStyle(
                                    color: Color.fromRGBO(158, 158, 158, 1)
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: smallGap,),
                      const Divider(thickness: 1,)
                    ],
                  ),
                ),

                // 핸드폰 번호 정보
                Container(
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.topLeft,
                        child: const Text(
                          '핸드폰번호',
                          style: TextStyle(
                            fontSize: fontSizeSmall,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          const Text(
                            '010-****-****', //모델로 받아와서 넣어주기
                            style: TextStyle(
                                fontSize: fontSizeLarge,
                                color: Color.fromRGBO(66, 126, 245, 1)
                            ),),
                          SizedBox(
                            height: 35,
                            width: 75,
                            child: OutlinedButton(
                              onPressed: (){},
                              style: OutlinedButton.styleFrom(
                                  side: const BorderSide(color: Color.fromRGBO(158, 158, 158, 1))
                              ),
                              child: const Text(
                                '수정',
                                style: TextStyle(
                                    color: Color.fromRGBO(158, 158, 158, 1)
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: smallGap,),
                      const Divider(thickness: 1,)
                    ],
                  ),
                ),

                // 비밀번호 정보
                Container(
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.topLeft,
                        child: const Text(
                          '비밀번호',
                          style: TextStyle(
                            fontSize: fontSizeSmall,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          const Text(
                            '비밀번호 변경하기',
                            style: TextStyle(
                                fontSize: fontSizeLarge,
                                color: Color.fromRGBO(66, 126, 245, 1)
                            ),),
                          SizedBox(
                            height: 35,
                            width: 75,
                            child: OutlinedButton(
                              onPressed: (){},
                              style: OutlinedButton.styleFrom(
                                  side: const BorderSide(color: Color.fromRGBO(158, 158, 158, 1))
                              ),
                              child: const Text(
                                '수정',
                                style: TextStyle(
                                    color: Color.fromRGBO(158, 158, 158, 1)
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: smallGap,),
                      const Divider(thickness: 1,)
                    ],
                  ),
                ),

                //회원 탈퇴 관리
                Container(
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.topLeft,
                        child: const Text(
                          '회원 탈퇴',
                          style: TextStyle(
                            fontSize: fontSizeSmall,
                          ),
                        ),
                      ),
                      SizedBox(height: smallGap,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          const Text(
                            '알바야를 더이상 이용하지 않을 경우, \n회원 탈퇴를 진행해주세요.',
                            style: TextStyle(
                              fontSize: fontSizeSmall,
                            ),),
                          SizedBox(
                            height: 35,
                            width: 75,
                            child: OutlinedButton(
                              onPressed: (){},
                              style: OutlinedButton.styleFrom(
                                  side: const BorderSide(color: Color.fromRGBO(158, 158, 158, 1))
                              ),
                              child: const Text(
                                '탈퇴',
                                style: TextStyle(
                                    color: Color.fromRGBO(158, 158, 158, 1)
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: smallGap,),
                      const Divider(thickness: 1,)
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }
  }
}

