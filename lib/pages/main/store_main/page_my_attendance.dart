import 'package:flutter/material.dart';
import 'package:my_worker_app/components/appbar/component_appbar_base.dart';
import 'package:my_worker_app/components/main/component_attendance_list.dart';
import 'package:my_worker_app/components/main/component_null_item.dart';
import 'package:my_worker_app/components/main/component_null_list.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/home/home_work_place_info_item.dart';
import 'package:my_worker_app/model/main/my_work_attendance_list_item.dart';
import 'package:my_worker_app/repository/home/repo_home.dart';

class PageMyAttendance extends StatefulWidget {
  const PageMyAttendance({
    super.key,
    required this.businessId,
    required this.pageNum,
  });

  final num businessId;
  final num pageNum;

  @override
  State<PageMyAttendance> createState() => _PageMyAttendanceState();
}

class _PageMyAttendanceState extends State<PageMyAttendance> {
  /// 출퇴근 리스트 보류  *
  List<MyWorkAttendanceListItem> _list = [];

  HomeWorkPlaceInfoItem? _infoItem;

  @override
  void initState() {
    super.initState();
    // 사업장 리스트 초기화
    _loadDetail;
    _infoItem;
    getList();
  }

  Future<void> _loadDetail() async {
    await RepoHome().getItem(widget.businessId).then((res) {
      setState(() {
        _infoItem = res.data;
      });
    }).catchError((err) {
      debugPrint(err);
    });
  }

  // [GET] 해당회원의 출퇴근 리스트 불러오기
  Future<void> getList() async {
    await RepoHome().getAttendanceList(widget.businessId).then((res) {
      setState(() {
        _list = res.list!;
      });
    }).catchError((err) {
      debugPrint(err.toString());
    });
  }

  /// 출퇴근 리스트 보류  *

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarBase(title: '출퇴근 기록'),
      body: Container(
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width / 1.1,
        height: MediaQuery.of(context).size.height / 3,
        margin: const EdgeInsets.only(top: 20, left: 20),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: colorLight,
            border: Border.all(
              color: colorNormal,
              width: 2,
            )),
        child: Column(children: [
          _list.isNotEmpty
              ? Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.all(10),
                      child: const Text(
                        '출/퇴근 리스트',
                        style: TextStyle(
                            color: colorNormal,
                            fontSize: 17,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Divider(
                      color: Colors.white,
                    ),
                  ],
                )
              : Container(
                  margin: const EdgeInsets.all(10),
                  child: const Text(
                    '',
                    style: TextStyle(
                        color: colorNormal,
                        fontSize: 17,
                        fontWeight: FontWeight.bold),
                  ),
                ),
          Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            margin: EdgeInsets.only(top: 5),
            // 사업장 리스트가 있으면 리스트 그리드뷰 , 없으면 없다고 안내 컴포넌트
            child: _list.isNotEmpty
                ? ListView.builder(
                    itemCount: _list.isNotEmpty ? _list.length : 1,
                scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext ctx, int idx) {
                      return ComponentAttendanceList(
                        myWorkAttendanceListItem: _list[idx],
                      );
                    })
                : const ComponentNullList(), // _list가 널일 때 표시할 UI 컴포넌트
            /** 리스트뷰  End**/
          ),
        ]),
      ),
    );
  }
}

/// 출퇴근 리스트 연동 보류 flutter 코드 *
// Column(
//             children:,[
//               _list.isNotEmpty
//                   ? Column(
//                 children: [
//                   Container(
//                     margin: const EdgeInsets.all(10),
//                     child: const Text(
//                       '출/퇴근 리스트',
//                       style: TextStyle(
//                           color: colorNormal,
//                           fontSize: 17,
//                           fontWeight: FontWeight.bold),
//                     ),
//                   ),Divider(color: Colors.white,),
//                 ],
//               )
//                   : Container(
//                 margin: const EdgeInsets.all(10),
//                 child: const Text(
//                   '',
//                   style: TextStyle(
//                       color: colorNormal,
//                       fontSize: 17,
//                       fontWeight: FontWeight.bold),
//                 ),
//               ),
//
//
//               Container(
//                 alignment: Alignment.center,
//                 padding: const EdgeInsets.only(left:20 ,right:20 ,),
//                 margin: EdgeInsets.only(top: 5),
//                 // 사업장 리스트가 있으면 리스트 그리드뷰 , 없으면 없다고 안내 컴포넌트
//                 child: _list.isNotEmpty
//                     ? ListView.builder(
//                     itemCount: _list.isNotEmpty ? _list.length : 1,
//                     shrinkWrap: true,
//                     physics: const NeverScrollableScrollPhysics(),
//                     itemBuilder: (BuildContext ctx, int idx) {
//                       return ComponentAttendanceList(
//                         myWorkAttendanceListItem: _list[idx],
//                       );
//                     })
//                     : const ComponentNullItem(), // _list가 널일 때 표시할 UI 컴포넌트
//                 /** 리스트뷰  End**/
//               ),
//             ]
//             )),
