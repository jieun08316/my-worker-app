import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/middleware/middleware_check_Location.dart';
import 'package:my_worker_app/model/home/home_work_place_info_item.dart';
import 'package:my_worker_app/model/home/location_request.dart';

import 'package:my_worker_app/pages/main/page_community.dart';
import 'package:my_worker_app/repository/home/repo_home.dart';

import 'package:my_worker_app/components/appbar/component_appbar_base.dart';

class TestPageHome extends StatefulWidget {
  const TestPageHome({super.key, required this.businessId});

  final num businessId;

  @override
  State<TestPageHome> createState() => _TestPageHomeState();
}

String _text = '출근하기'; // 기본 텍스트 설정
String _textE = '퇴근하기'; // 기본 텍스트 설정
bool isButtonEnabled = false;
HomeWorkPlaceInfoItem? _infoItem;
Position? currentPosition;
LocationRequest? _request;

class _TestPageHomeState extends State<TestPageHome> {
  @override
  void initState() {
    super.initState();
    _determinePosition(); //실행만 해서는 안들어감
    _determinePosition().then((value) => {
          setState(() {
            _loadDetail();
            currentPosition = value;
          })
        });
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;
    bool isLocationMatched = false;
    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition();
  }

  Future<void> sendAttendance() async {
    await RepoHome().doAttendance(widget.businessId).then((res) {
      setState(() {
        _request = res.data;
        print('성공');
      });
    }).catchError((errCode) {
      debugPrint(errCode);
      print('실패');

    });
  }

// [GET] 사업장 상세보기 API 연동
// 이 코드랑 위에 _infoItem은 다른 페이지에서도 계속 들고다녀야 함
// 알바생은 1:N 구조이기 때문
  Future<void> _loadDetail() async {
    await RepoHome().getItem(widget.businessId).then((res) {
      setState(() {
        _infoItem = res.data;
      });
    }).catchError((errCode) {
      debugPrint(errCode);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //앱바
      appBar: ComponentAppbarBase(
        title: '${_infoItem?.businessName} 홈',
      ),
      //바디 부분 buildBody로 위젯처리함
      body: SingleChildScrollView(
        child: _buildBody(context),
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    // double screenWidth = MediaQuery.of(context).size.width - 320 / 2.0;

    //상세 정보가 로딩이 되지 않음을 확인 하는 부분
    //_infoItem(사업장 정보)가  null인지 확인
    if (_infoItem == null) {
      //만약 null인 경우 로딩 중임을 알리는 데이터 없음 안내
      return Center(
          child: Container(
        child: Text("데이터가 없습니다."),
      ));
    } else {
      // 데이터 있을 경우 하기 내용 반환
      return Container(
        child: Column(
          children: [
            // 사업장 정보 노출
            // 주소의 경우 길어질 가능성이 많아서 오른쪽 여백 더 남겨둠
            // 현재 데이터베이스에 목업으로 되어있어서 짧음
            Container(
              height: MediaQuery.of(context).size.height / 5 + 20,
              padding: EdgeInsets.all(10),
              child: ElevatedButton(
                onPressed: () {
                  // 이동하고 싶은 페이지 있으면 추가
                  // ex) 근무지 정보 전부 보기
                },
                style: ElevatedButton.styleFrom(
                    shape: BeveledRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    side: const BorderSide(
                      color: colorNormal,
                      width: 2,
                    )),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    //사업장 아이콘
                    Container(
                      child: const Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          // 아이콘
                          Icon(
                            Icons.business_sharp,
                            color: colorNormal,
                            size: 80,
                          ),
                        ],
                      ),
                    ),

                    // 사업장 정보 문구
                    Container(
                      margin: EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            _infoItem!.businessName,
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: colorBaseBlack),
                          ),
                          Text('대표자명 ${_infoItem!.ownerName}',
                              style: TextStyle(fontSize: 13, color: colorGrey)),
                          Text('업종 ${_infoItem!.businessType}',
                              style: TextStyle(fontSize: 13, color: colorGrey)),
                          Text('전화번호 ${_infoItem!.businessPhoneNumber}',
                              style: TextStyle(fontSize: 13, color: colorGrey)),
                          Text('주소 ${_infoItem!.reallyLocation}',
                              style: TextStyle(fontSize: 13, color: colorGrey)),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),

            // 출근하기
            /** 2번 컨테이너 **/
            Divider(color: colorNormal, thickness: 2),
            Container(
              margin: EdgeInsets.only(top: 5, bottom: 5),
              width: MediaQuery.of(context).size.width / 1,
              height: 100,
              child: Column(
                children: [
                  Text(
                    '${_infoItem!.businessName}',
                    style: TextStyle(
                      fontSize: 20,
                      color: colorNormal,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    child: Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(right: 20),
                          child: ElevatedButton(
                            onPressed: () async {

                              isButtonEnabled = true; //
                                setState(() {
                                  _text = '출근완료'; // 버튼 텍스트 변경

                                });
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: Text('알림'),
                                      content: Text('현재 위치가 일치하지 않습니다.'),
                                      actions: [
                                        TextButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: Text('확인'),
                                        ),
                                      ],
                                    );
                                  },
                                );
                              },
                            style: ElevatedButton.styleFrom(
                              primary: colorNormal,
                              onPrimary: colorDark,
                            ),
                            child: Container(
                              padding: EdgeInsets.all(10),
                              width: MediaQuery.of(context).size.width / 3,
                              child: Center(
                                child: Text(
                                  _text,
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                        ),

                        /** 퇴근 버튼 **/
                        // ElevatedButton(
                        //   onPressed:
                        //   isButtonEnabled // 버튼 활성화 여부에 따라 onPressed 설정
                        //       ? () async {
                        //     // 2번 버튼을 눌렀을 때 동작
                        //
                        //     isButtonEnabled = true; //
                        //     if () {
                        //       setState(() {
                        //         _textE = '퇴근완료'; // 버튼 텍스트 변경
                        //       });
                        //     } else {
                        //       showDialog(
                        //         context: context,
                        //         builder: (BuildContext context) {
                        //           return AlertDialog(
                        //             title: Text('알림'),
                        //             content: Text('현재 위치가 일치하지 않습니다.'),
                        //             actions: [
                        //               TextButton(
                        //                 onPressed: () {
                        //                   Navigator.of(context).pop();
                        //                 },
                        //                 child: Text('확인'),
                        //               ),
                        //             ],
                        //           );
                        //         },
                        //       );
                        //     }
                        //   }
                        //       : null, // 버튼 비활성화 상태일 때 onPressed를 null로 설정
                        //   style: ElevatedButton.styleFrom(
                        //     primary: colorNormal,
                        //     onPrimary: colorDark,
                        //   ),
                        //   child: Container(
                        //     padding: EdgeInsets.all(10),
                        //     width: MediaQuery.of(context).size.width / 3,
                        //     child: Center(
                        //       child: Text(
                        //         _textE,
                        //         style: TextStyle(
                        //             fontSize: 20,
                        //             fontWeight: FontWeight.bold,
                        //             color: Colors.white),
                        //       ),
                        //     ),
                        //   ),
                        // ),
                      ],
                    )
                    // Row(
                    //   children: [
                    //     Container(
                    //       margin: EdgeInsets.only(right: 20),
                    //       child: ElevatedButton(
                    //         onPressed: () async {
                    //           bool isLocationMatched = await checkLocation(currentPosition!.longitude, currentPosition!.latitude);// 위치 확인
                    //           isButtonEnabled = true; //
                    //           if (isLocationMatched) {
                    //             setState(() {
                    //               _text = '출근완료'; // 버튼 텍스트 변경
                    //             });
                    //           } else {
                    //             showDialog(
                    //               context: context,
                    //               builder: (BuildContext context) {
                    //                 return AlertDialog(
                    //                   title: Text('알림'),
                    //                   content: Text('현재 위치가 일치하지 않습니다.'),
                    //                   actions: [
                    //                     TextButton(
                    //                       onPressed: () {
                    //                         Navigator.of(context).pop();
                    //                       },
                    //                       child: Text('확인'),
                    //                     ),
                    //                   ],
                    //                 );
                    //               },
                    //             );
                    //           }
                    //         },
                    //         style: ElevatedButton.styleFrom(
                    //           primary: colorNormal,
                    //           onPrimary: colorDark,
                    //         ),
                    //         child: Container(
                    //           padding: EdgeInsets.all(10),
                    //           width: MediaQuery.of(context).size.width / 3,
                    //           child: Center(
                    //             child: Text(
                    //               _text,
                    //               style: TextStyle(
                    //                   fontSize: 20,
                    //                   fontWeight: FontWeight.bold,
                    //                   color: Colors.white),
                    //             ),
                    //           ),
                    //         ),
                    //       ),
                    //     ),
                    //     ElevatedButton(
                    //       onPressed:
                    //       isButtonEnabled // 버튼 활성화 여부에 따라 onPressed 설정
                    //           ? () async {
                    //         // 2번 버튼을 눌렀을 때 동작
                    //         bool isLocationMatched = await checkLocation(currentPosition!.longitude, currentPosition!.latitude);// 위치 확인
                    //
                    //         if (isLocationMatched) {
                    //           setState(() {
                    //             _textE = '퇴근완료'; // 버튼 텍스트 변경
                    //           });
                    //         } else {
                    //           showDialog(
                    //             context: context,
                    //             builder: (BuildContext context) {
                    //               return AlertDialog(
                    //                 title: Text('알림'),
                    //                 content: Text('현재 위치가 일치하지 않습니다.'),
                    //                 actions: [
                    //                   TextButton(
                    //                     onPressed: () {
                    //                       Navigator.of(context).pop();
                    //                     },
                    //                     child: Text('확인'),
                    //                   ),
                    //                 ],
                    //               );
                    //             },
                    //           );
                    //         }
                    //       }
                    //           : null, // 버튼 비활성화 상태일 때 onPressed를 null로 설정
                    //       style: ElevatedButton.styleFrom(
                    //         primary: colorNormal,
                    //         onPrimary: colorDark,
                    //       ),
                    //       child: Container(
                    //         padding: EdgeInsets.all(10),
                    //         width: MediaQuery.of(context).size.width / 3,
                    //         child: Center(
                    //           child: Text(
                    //             _textE,
                    //             style: TextStyle(
                    //                 fontSize: 20,
                    //                 fontWeight: FontWeight.bold,
                    //                 color: Colors.white),
                    //           ),
                    //         ),
                    //       ),
                    //     ),
                    //   ],
                    // )
                    // ElevatedButton(
                    //   onPressed: () {
                    //     setState(() async {
                    //       /**GPS 기능 넣어야 함 **/
                    //
                    //       /**GPS 기능 끝 **/
                    //     });
                    //
                    //     Text(
                    //       _text = '퇴근하기',
                    //       textAlign: TextAlign.center,
                    //       style: TextStyle(fontSize: 26, letterSpacing: 2),
                    //     );
                    //   },
                    //   style: ElevatedButton.styleFrom(
                    //     primary: colorNormal,
                    //     onPrimary: colorDark,
                    //   ),
                    //   child: Container(
                    //     padding: EdgeInsets.all(10),
                    //     width: MediaQuery.of(context).size.width / 3,
                    //     child: Center(
                    //       child: Text(
                    //         _text,
                    //         style: TextStyle(
                    //             fontSize: 20,
                    //             fontWeight: FontWeight.bold,
                    //             color: Colors.white),
                    //       ),
                    //     ),
                    //   ),
                    // ),
                  )
                ],
              ),
            ),

            Divider(color: colorNormal, thickness: 2),
            /** 마지막 컨테이너 **/

            //퀵메뉴
            Container(
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width / 1.1,
              margin: const EdgeInsets.only(top: 20),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: colorLight,
                  border: Border.all(
                    color: colorNormal,
                    width: 2,
                  )),
              child: Column(children: [
                Container(
                  margin: const EdgeInsets.all(10),
                  child: const Text(
                    '바로가기',
                    style: TextStyle(
                        color: colorNormal,
                        fontSize: 17,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Divider(color: colorNormal),
                Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width - 32 / 1.1,
                  height: MediaQuery.of(context).size.height / 3 + 10,
                  margin: EdgeInsets.only(left: 20, right: 20),
                  child: GridView(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        childAspectRatio: 0.9,
                        mainAxisSpacing: 20,
                        crossAxisSpacing: 20),
                    children: [
                      /**첫번째 버튼 퀵메뉴 시작**/
                      ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PageCommunity()));
                        },
                        style: ElevatedButton.styleFrom(
                          shape: CircleBorder(),
                          primary: colorNormal,
                          onPrimary: colorDark,
                        ),
                        child: Column(
                          children: [
                            /**버튼 내용 수정**/
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              width: 100,
                              height: 70,
                              child: Column(
                                children: [
                                  Image.asset(
                                    'assets/img/salary.png',
                                    color: Colors.white,
                                  ),
                                  Text('급여내역',
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                      ))
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),

                      /**2번 버튼 퀵메뉴 시작**/
                      ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PageCommunity()));
                        },
                        style: ElevatedButton.styleFrom(
                          shape: CircleBorder(),
                          primary: colorNormal,
                          onPrimary: colorDark,
                        ),
                        child: Column(
                          children: [
                            /**버튼 내용 수정**/
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              width: 100,
                              height: 70,
                              child: Column(
                                children: [
                                  Image.asset(
                                    'assets/img/stock.png',
                                    color: Colors.white,
                                  ),
                                  Text('재고관리',
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                      ))
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      /**3번 버튼 퀵메뉴 시작**/
                      ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PageCommunity()));
                        },
                        style: ElevatedButton.styleFrom(
                          shape: CircleBorder(),
                          primary: colorNormal,
                          onPrimary: colorDark,
                        ),
                        child: Column(
                          children: [
                            /**버튼 내용 수정**/
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              width: 100,
                              height: 70,
                              child: Column(
                                children: [
                                  Image.asset(
                                    'assets/img/contract.png',
                                    color: Colors.white,
                                  ),
                                  Text('근로계약서',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          letterSpacing: -1))
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),

                      /**4번 버튼 퀵메뉴 시작**/
                      ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PageCommunity()));
                        },
                        style: ElevatedButton.styleFrom(
                          shape: CircleBorder(),
                          primary: colorNormal,
                          onPrimary: colorDark,
                        ),
                        child: Column(
                          children: [
                            /**버튼 내용 수정**/
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              width: 100,
                              height: 70,
                              child: Column(
                                children: [
                                  Image.asset(
                                    'assets/img/stock.png',
                                    color: Colors.white,
                                  ),
                                  Text('급여예상',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold))
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      /**5번 버튼 퀵메뉴 시작**/
                      ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PageCommunity()));
                        },
                        style: ElevatedButton.styleFrom(
                          shape: CircleBorder(),
                          primary: colorNormal,
                          onPrimary: colorDark,
                        ),
                        child: Column(
                          children: [
                            /**버튼 내용 수정**/
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              width: 100,
                              height: 70,
                              child: Column(
                                children: [
                                  Image.asset(
                                    'assets/img/stock.png',
                                    color: Colors.white,
                                  ),
                                  Text('재고관리',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold))
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      /**6번 버튼 퀵메뉴 시작**/
                      ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PageCommunity()));
                        },
                        style: ElevatedButton.styleFrom(
                          shape: CircleBorder(),
                          primary: colorNormal,
                          onPrimary: colorDark,
                        ),
                        child: Column(
                          children: [
                            /**버튼 내용 수정**/
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              width: 100,
                              height: 70,
                              child: Column(
                                children: [
                                  Image.asset(
                                    'assets/img/exit.png',
                                    color: Colors.white,
                                  ),
                                  Text('알바나가기',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          letterSpacing: -1))
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ]),
            ),
          ],
        ),
      );
    }
  }
}
