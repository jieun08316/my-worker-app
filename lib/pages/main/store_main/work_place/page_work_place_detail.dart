import 'package:flutter/material.dart';
import 'package:my_worker_app/components/appbar/component_appbar_base.dart';
import 'package:my_worker_app/components/work/component_work_place.dart';

class PageWorkPlaceDetail extends StatefulWidget {
  const PageWorkPlaceDetail({super.key});

  @override
  State<PageWorkPlaceDetail> createState() => _PageWorkPlaceDetailState();
}

class _PageWorkPlaceDetailState extends State<PageWorkPlaceDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarBase(title: '아아ㅏ'),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              child: ComponentWorkPlace(),
            ),
            Column(
              children: [
                Container(
                  padding: EdgeInsets.all(25),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(color: Colors.grey.withOpacity(0.3)),
                    ),
                  ),
                  child: Container(
                    child: TextButton(
                      onPressed: () {},
                      child: Text(
                        '근태현황',
                        style: TextStyle(
                          fontSize: 16,
                          color: Color.fromRGBO(100, 100, 100, 1),
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
