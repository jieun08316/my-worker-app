import 'package:flutter/material.dart';
import 'package:my_worker_app/components/components_loading.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/home/home_work_place_info_item.dart';
import 'package:my_worker_app/model/home/home_work_place_info_result.dart';
import 'package:my_worker_app/model/main/my_work_attendance_list_item.dart';
import 'package:my_worker_app/pages/main/store_main/manual_manage/page_manual_list.dart';
import 'package:my_worker_app/pages/main/store_main/page_home.dart';
import 'package:my_worker_app/pages/main/store_main/page_manual.dart';
import 'package:my_worker_app/pages/main/store_main/page_my_attendance.dart';
import 'package:my_worker_app/pages/main/store_main/page_transition.dart';
import 'package:my_worker_app/pages/main/store_main/transition/page_transition_list.dart';
import 'package:my_worker_app/repository/home/repo_home.dart';

// 맨 처음 페이지, 하단바 구성
class PageIndex extends StatefulWidget {
  const PageIndex({
    super.key,
    required this.id,
  });

  final num id;

  @override
  State<PageIndex> createState() => _PageIndexState();
}


// 사업장 선택 후 페이지 이동 안됐던 이유가
// API 데이터 불러오는 코드가 없음 (dio)
// 현재 페이지 코드의 _loadBusinessDetail()가 없었음

class _PageIndexState extends State<PageIndex> {

  HomeWorkPlaceInfoItem? _infoItem;
  List<Widget> _widgetOptions = [];
  MyWorkAttendanceListItem? _listItem;

  @override
  void initState() {
    super.initState();
    _loadBusinessDetail();

  }

  //[GET] 사업장 상세보기 API 연동
  // Future<void> _loadBusinessDetail() async {
  //   // RepoMember를 통해 id에 해당하는 MemberResponse를 가져옵니다.
  //   await RepoHome().getItem(widget.id)
  //       .then((res) => setState(() {
  //     _infoItem = res.data;
  //     _widgetOptions = [
  //       PageHome(id: _infoItem!.id), /** 홈 **/
  //       PageMyAttendance(id: _listItem!.id, pageNum:_listItem!.pageNum,), //id: _listItem!.id, ///** 출퇴근 기록**/
  //       PageTransition(), /** 인수인계 **/
  //       PageManual(), /** 메뉴얼  **/
  //     ];
  //   }));
  // }

  Future<void> _loadBusinessDetail() async {
    try {
      HomeWorkPlaceInfoResult res = await RepoHome().getItem(widget.id);
      setState(() {
        _infoItem = res.data;
        _widgetOptions = [
          PageHome(id: _infoItem!.id), /** 홈 **/
          PageMyAttendance(businessId: _listItem?.businessId ?? 0, pageNum: _listItem?.pageNum ?? 0),
          PageTransitionList(BusinessId: widget.id,), /** 인수인계 **/
          PageManualList(), /** 메뉴얼  **/
        ];
      });
        } catch (e) {
      // 에러 처리
      print('Error fetching data: $e');
      // 필요에 따라 사용자에게 에러를 보여줄 수도 있습니다.
    }
  }
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  final List<BottomNavigationBarItem> _icons = [
    const BottomNavigationBarItem(icon: Icon(Icons.home), label: '홈',),
    const BottomNavigationBarItem(icon: Icon(Icons.app_registration_sharp), label: '출/퇴근 기록',),
    const BottomNavigationBarItem(icon: Icon(Icons.developer_board), label: '인수인계',),
    const BottomNavigationBarItem(icon: Icon(Icons.menu_book), label: '메뉴얼',),
  ];
  //
  // // 이 코드 주석 처리하고 api 불러올 때 setState에 넣었어요
  //  List<Widget> _widgetOptions = <Widget>[
  //
  //    PageHome(id: _infoItem!.id,), /** 홈 **/
  //    PageMyAttendance(), /** 출퇴근 기록**/
  //    PageTransition(), /** 인수인계 **/
  //    PageManual(), /** 메뉴얼  **/
  //  ];
  //


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // 앱바 삭제 => 앱바는 인덱스 페이지에 넣게 되면
      // 이동하는 페이지마다 앱바 영역까지 생각해서 데이터 넣어야하기 때문에
      // index에서는 앱바 빼고 페이지별로 넣는 걸로 했어요
      // 이것때문에 다음 페이지 가는 거 오류떴어여 => 랜더링 에러 부분 해결
      // 이전 오류들 캡쳐 한걸로 설명

      // 바디 내용 수정 => 랜더링 에러 부분 해결
      // 이건 못 불러올수도 있으니 null처리 해주는거
      body: _widgetOptions.isNotEmpty ? _widgetOptions.elementAt(_selectedIndex) : ComponentsLoading(),


      //바텀 네비게이션바
      bottomNavigationBar: BottomNavigationBar(
        selectedLabelStyle: const TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 13,
        ),
        backgroundColor: Colors.blue,
        items: _icons,
        currentIndex: _selectedIndex,
        selectedItemColor: colorNormal,
        unselectedItemColor: colorDark,
        onTap: _onItemTapped,
      ),
    );
  }
}
