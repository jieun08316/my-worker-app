import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/function/token_lib.dart';
import 'package:my_worker_app/middleware/middleware_login_check.dart';
import 'package:my_worker_app/model/login/login_request.dart';
import 'package:my_worker_app/pages/main/page_part_time_index.dart';
import 'package:my_worker_app/pages/member/page_join.dart';
import 'package:my_worker_app/repository/login/repo_login.dart';



class PageLogin extends StatefulWidget {
  const PageLogin({Key? key}) : super(key: key);

  @override
  State<PageLogin> createState() => _PageLoginState();
}

class _PageLoginState extends State<PageLogin> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _doLogin(LoginRequest loginRequest) async {
    await RepoLogin().doLogin(loginRequest).then((res) {
      TokenLib.setMemberName(res.data.name);
      TokenLib.setMemberToken(res.data.token);
      MiddlewareLoginCheck().check(context);
      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const PagePartTimeIndex())
      );
    }).catchError((err) {
      print('실패임');
      debugPrint(err);
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _login(context),
    );
  }

  Widget _login(BuildContext context) {
    return Scaffold(
      backgroundColor: colorNormal,
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 4,
                    alignment: Alignment.center,
                    child: Image.asset(
                      'assets/img/logo_t.png',
                      width: 200,
                    ),
                  ),
                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                      BorderRadius.only(topRight: Radius.circular(80)),
                ),
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.all(50),
                      child: Text(
                        '로그인',
                        style: TextStyle(
                            fontSize: 30,
                            color: colorNormal,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Column(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 1.5,
                          padding: EdgeInsets.only(bottom: 10),
                          child: FormBuilder(
                            key: _formKey,
                            autovalidateMode: AutovalidateMode.disabled,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                FormBuilderTextField(
                                  maxLength: 20,
                                  name: 'username',
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    hintText: '아이디',
                                    hintStyle: TextStyle(height: 1.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(50),
                                        //모서리 둥글게
                                        borderSide: BorderSide(
                                          color: colorNormal,
                                          width: 2.0,
                                        )),
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(50),
                                        //모서리 둥글게
                                        borderSide: BorderSide(
                                          color: colorNormal,
                                          width: 2.0,
                                        )),
                                  ),
                                  textInputAction: TextInputAction.next,
                                  validator: (String? value) {
                                    if (value == null || value.isEmpty) {
                                      return '필수값 입니다. 다시 확인해 주세요.';
                                    }
                                    return null;
                                  },
                                ),
                                FormBuilderTextField(
                                  maxLength: 20,
                                  name: 'password',
                                  keyboardType: TextInputType.text,
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    hintText: '비밀번호',
                                    hintStyle: TextStyle(height: 1.0),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(30),
                                      borderSide: BorderSide(
                                          color: colorNormal, width: 2),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(30),
                                      borderSide: BorderSide(
                                          color: colorNormal, width: 2),
                                    ),
                                  ),
                                  textInputAction: TextInputAction.next,
                                  validator: (String? value) {
                                    if (value == null || value.isEmpty) {
                                      return '필수값 입니다. 다시 확인해 주세요.';
                                    }
                                    return null;
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.2,
                          height: MediaQuery.of(context).size.height / 9,
                          padding: const EdgeInsets.all(20),
                          child: ElevatedButton(
                            onPressed: () {
                              if (_formKey.currentState!.saveAndValidate()) {
                                LoginRequest loginRequest = LoginRequest(
                                  _formKey
                                      .currentState!.fields['username']!.value,
                                  _formKey
                                      .currentState!.fields['password']!.value,
                                );
                                _doLogin(loginRequest);
                              }
                            },
                            style: ElevatedButton.styleFrom(
                                primary: colorNormal,
                                onPrimary: colorDark,
                                side:
                                    const BorderSide(color: colorNormal, width: 2.0)),
                            child: const Text(
                              '로그인',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ), Container(
                          width: MediaQuery.of(context).size.width / 2,

                          height: 50,
                          child: TextButton(
                            onPressed: () {},
                            child: Text('아이디 / 비밀번호 찾기',
                              style: TextStyle(
                                  fontSize: 16
                              ),),
                          ),
                        ),
                        Container(
                            width: MediaQuery.of(context).size.width / 2,
                            height: 50,
                            child: TextButton(
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => PageJoin()));
                              },
                              child: Text('회원가입',
                                style: TextStyle(
                                    fontSize: 16),
                              ),
                            )
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
