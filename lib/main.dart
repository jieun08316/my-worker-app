import 'package:flutter/material.dart';
import 'package:my_worker_app/pages/member/page_login.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '알바야-알바앱',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'Pretendard',
        colorScheme: ColorScheme.fromSeed(seedColor: const Color.fromRGBO(63, 114, 175, 1)),
        useMaterial3: true,
      ),
      home: PageLogin(),
    );
  }
}

