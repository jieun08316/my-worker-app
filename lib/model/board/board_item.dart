class BoardItem {
  num id;
  num memberId;
  String memberImgUrl;
  String memberNickName;
  String topicA;
  String topicB;
  String dateBoard;
  num viewCount;
  num likeCount;

  BoardItem(
      this.id,
      this.memberId,
      this.memberImgUrl,
      this.memberNickName,
      this.topicA,
      this.topicB,
      this.dateBoard,
      this.viewCount,
      this.likeCount,
      );
  factory BoardItem.fromJson(Map<String, dynamic> json) {
    return BoardItem(
      json['id'],
      json['memberId'],
      json['memberImgUrl'],
      json['memberNickName'],
      json['topicA'],
      json['topicB'],
      json['dateBoard'],
      json['viewCount'],
      json['likeCount'],
    );
  }




}