class MemberResponse {
  num id;
  String memberRatingName; //회원 상태
  String gradeName; //회원 등급
  String username; //아이디
  //String password; //비밀 번호
  String imgUrl;
  String nickName; //닉네임
  String name; //이름
  String dateBirth; //생년 월일
  String phoneNumber; //핸드폰 번호
  String gender; //성별
  String email; // 이메일
  //String dateUser; //회원 생성 일시

  MemberResponse(
      this.id,
      this.memberRatingName,
      this.gradeName,
      this.username,
      //this.password,
      this.imgUrl,
      this.nickName,
      this.name,
      this.dateBirth,
      this.phoneNumber,
      this.gender,
      this.email,
      //this.dateUser,
      );

  factory MemberResponse.fromJson(Map<String, dynamic> json) {
    return MemberResponse (
      json['id'],
      json['memberRatingName'],
      json['gradeName'],
      json['username'],
      //json['password'],
      json['imgUrl'],
      json['nickName'],
      json['name'],
      json['dateBirth'],
      json['phoneNumber'],
      json['gender'],
      json['email'],
      //json['dateUser'],

    );
  }


}