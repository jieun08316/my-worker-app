import 'package:my_worker_app/model/main/my_work_attendance_list_item.dart';

class MyWorkAttendanceListResult {
  String msg;
  num code;
  List<MyWorkAttendanceListItem>? list;

  MyWorkAttendanceListResult(
      this.msg,
      this.code,
      this.list
      );

  factory MyWorkAttendanceListResult.fromJson(Map<String, dynamic> json) {
    return MyWorkAttendanceListResult(
      json['msg'],
      json['code'],
      json['list'] != null ? (json['list'] as List).map((e) => MyWorkAttendanceListItem.fromJson(e)).toList() : [],
    );
  }
}