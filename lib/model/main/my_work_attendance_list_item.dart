class MyWorkAttendanceListItem {
  num businessId;
  num pageNum;
  String businessMemberPosition;
  String businessMemberName;
  String dateWork;
  String isLateness;
  String startWork;
  String endWork;
  num totalWorkTime;

  MyWorkAttendanceListItem(
    this.businessId,
    this.pageNum,
    this.businessMemberPosition,
    this.businessMemberName,
    this.dateWork,
    this.isLateness,
    this.startWork,
    this.endWork,
    this.totalWorkTime,
  );

  factory MyWorkAttendanceListItem.fromJson(Map<String, dynamic> json) {
    return MyWorkAttendanceListItem(
      json['businessId'],
      json['pageNum'],
      json['businessMemberPosition'],
      json['businessMemberName'],
      json['dateWork'],
      json['isLateness'],
      json['startWork'],
      json['endWork'],
      json['totalWorkTime'],
    );
  }
}
