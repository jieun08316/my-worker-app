class LocationRequest {
  num? latitude;
  num? longitude;

  LocationRequest({this.latitude, this.longitude,});

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = <String, dynamic>{};
    data['latitude'] = latitude;
    data['longitude'] = longitude;

    return data;
  }
}
