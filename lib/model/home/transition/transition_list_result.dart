
import 'transition_list.dart';

class TransitionListResult {
  String msg;
  num code;
  List<TransitionList>? list;

  TransitionListResult(
      this.msg,
      this.code,
      this.list
      );

  factory TransitionListResult.fromJson(Map<String, dynamic> json) {
    return TransitionListResult(
        json['msg'],
        json['code'],
        json['list'] != null ? (json['list'] as List).map((e) => TransitionList.fromJson(e)).toList() : [],
    );
  }
}