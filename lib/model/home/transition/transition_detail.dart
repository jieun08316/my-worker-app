class TransitionDetail {
  String content;

  TransitionDetail(this.content);

  factory TransitionDetail.fromJson(Map<String, dynamic> json) {
    return TransitionDetail(
      json['content']
    );
  }
}