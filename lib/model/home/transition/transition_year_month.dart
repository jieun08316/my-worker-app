class TransitionYearMonth {
  int year;
  int month;

  TransitionYearMonth (this.year, this.month);

  factory TransitionYearMonth.fromJson(Map<String, dynamic> json) {
    return TransitionYearMonth(
      json['year'],
      json['month']
    );
  }
}