
import 'transition_year_month.dart';

class TransitionYearMonthResult {
  String msg;
  num code;
  List<TransitionYearMonth>? list;

  TransitionYearMonthResult(
      this.msg,
      this.code,
      this.list
      );

  factory TransitionYearMonthResult.fromJson(Map<String, dynamic> json) {
    return TransitionYearMonthResult(
        json['msg'],
        json['code'],
        json['list'] != null ? (json['list'] as List).map((e) => TransitionYearMonth.fromJson(e)).toList() : [],
    );
  }
}