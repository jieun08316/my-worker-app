class TransitionRegistRequest {
  String content;

  TransitionRegistRequest(this.content);

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();

    data['content'] = content;

    return data;
  }
}