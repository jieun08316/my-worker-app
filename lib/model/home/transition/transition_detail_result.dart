
import 'transition_detail.dart';

class TransitionDetailResult {
  String msg;
  num code;
  TransitionDetail data;

  TransitionDetailResult(
      this.msg,
      this.code,
      this.data
      );

  factory TransitionDetailResult.fromJson(Map<String, dynamic> json) {
    return TransitionDetailResult(
        json['msg'],
        json['code'],
        TransitionDetail.fromJson(json['data'])
    );
  }
}