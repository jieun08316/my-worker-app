class TransitionList {
  num id;
  String memberName;
  String dateTransition;
  String content;
  String? isFinish;

  TransitionList(
      this.id,
      this.memberName,
      this.dateTransition,
      this.content,
      this.isFinish
  );

  factory TransitionList.fromJson(Map<String, dynamic> json) {
    return TransitionList(
      json['id'],
      json['memberName'],
      json['dateTransition'],
      json['content'],
      json['isFinish'],
    );
  }
}