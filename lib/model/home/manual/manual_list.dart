class ManualList {
  num id;
  String memberName;
  String title;
  String dateManual;

  ManualList (
      this.id,
      this.memberName,
      this.title,
      this.dateManual,
  );

  factory ManualList.fromJson(Map<String, dynamic> json) {
    return ManualList(
      json['id'],
      json['memberName'],
      json['title'],
      json['dateManual'],
    );
  }
}