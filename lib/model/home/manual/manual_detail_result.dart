
import 'package:my_worker_app/model/home/manual/manual_detail.dart';

class ManualDetailResult {
  String msg;
  num code;
  ManualDetail data;

  ManualDetailResult(
      this.msg,
      this.code,
      this.data
      );

  factory ManualDetailResult.fromJson(Map<String, dynamic> json) {
    return ManualDetailResult(
        json['msg'],
        json['code'],
        ManualDetail.fromJson(json['data'])
    );
  }
}