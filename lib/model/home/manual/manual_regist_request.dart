class ManualRegistRequest {
  String title;
  String content;
  String? manualImgUrl;

  ManualRegistRequest(this.title, this.content, this.manualImgUrl);

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();

    data['title'] = title;
    data['content'] = content;
    data['manualImgUrl'] = manualImgUrl;

    return data;
  }
}