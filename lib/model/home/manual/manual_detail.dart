class ManualDetail {
  num id;
  String memberName;
  String dateManual;
  String title;
  String content;
  String manualImgUrl;

  ManualDetail (
      this.id,
      this.memberName,
      this.dateManual,
      this.title,
      this.content,
      this.manualImgUrl
  );

  factory ManualDetail.fromJson (Map<String, dynamic> json) {
    return ManualDetail(
      json['id'],
      json['memberName'],
      json['dateManual'],
      json['title'],
      json['content'],
      json['manualImgUrl'],
    );
  }
}