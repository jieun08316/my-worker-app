
import 'package:my_worker_app/model/home/manual/manual_list.dart';

class ManualListResult {
  num totalCount;
  num totalPage;
  num currentPage;
  List<ManualList>? list;

  ManualListResult(this.totalCount, this.totalPage, this.currentPage, this.list);

  factory ManualListResult.fromJson(Map<String, dynamic> json) {
    return ManualListResult(
      json['totalCount'],
      json['totalPage'],
      json['currentPage'],
      json['list'] != null ? (json['list'] as List).map((e) => ManualList.fromJson(e)).toList() : [],
    );
  }
}