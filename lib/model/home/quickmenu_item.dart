import 'package:intl/intl.dart';

class QuickMenu {
  String menu;
  String imgUrl;


  QuickMenu(
      this.menu,
      this.imgUrl,
      );

  factory QuickMenu.fromJson(Map<String, dynamic> json) {
    return QuickMenu(
      json['menu'],
      json['imgUrl'],
    );
  }}