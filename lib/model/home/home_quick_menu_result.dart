import 'package:my_worker_app/model/main/my_work_place_item.dart';

class HomeQuickMenuResult {
  String msg;
  num code;
  List<MyWorkPlaceItem>? list;

  HomeQuickMenuResult(
      this.msg,
      this.code,
      this.list
      );

  factory HomeQuickMenuResult.fromJson(Map<String, dynamic> json) {
    return HomeQuickMenuResult(
        json['msg'],
        json['code'],
        json['list'] != null ? (json['list'] as List).map((e) => MyWorkPlaceItem.fromJson(e)).toList() : [],
    );
  }
}