import 'package:my_worker_app/model/home/location_request.dart';
import 'package:my_worker_app/model/home/location_response.dart';

class LocationResult {
  String msg;
  int code;
  LocationRequest data;

  LocationResult(
      this.msg,
      this.code,
      this.data
      );

  factory LocationResult.fromJson(Map<String, dynamic> json) {
    return LocationResult(
        json['msg'],
        json['code'],
        json['data']);

  }
}