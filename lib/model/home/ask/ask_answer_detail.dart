class AskAnswerDetail {
  num id;
  String answerTitle;
  String answerContent;
  String answerImgUrl;
  String dateAnswer;

  AskAnswerDetail(
      this.id,
      this.answerTitle,
      this.answerContent,
      this.answerImgUrl,
      this.dateAnswer
      );

  factory AskAnswerDetail.fromJson(Map<String, dynamic> json) {
    return AskAnswerDetail(
      json['id'],
      json['answerTitle'],
      json['answerContent'],
      json['answerImgUrl'],
      json['dateAnswer'],
    );
  }
}