import 'package:my_worker_app/model/home/ask/ask_answer_detail.dart';

class AskAnswerDetailResult {
  String msg;
  num code;
  AskAnswerDetail data;

  AskAnswerDetailResult(
      this.msg,
      this.code,
      this.data
      );

  factory AskAnswerDetailResult.fromJson(Map<String, dynamic> json) {
    return AskAnswerDetailResult(
        json['msg'],
        json['code'],
        AskAnswerDetail.fromJson(json['data'])
    );
  }
}