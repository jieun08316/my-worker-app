import 'package:my_worker_app/model/home/home_work_place_info_item.dart';
class HomeWorkPlaceInfoResult {
  String msg;
  num code;
  HomeWorkPlaceInfoItem data;

  HomeWorkPlaceInfoResult(
      this.msg,
      this.code,
      this.data
      );

  factory HomeWorkPlaceInfoResult.fromJson(Map<String, dynamic> json) {
    return HomeWorkPlaceInfoResult(
      json['msg'],
      json['code'],
      HomeWorkPlaceInfoItem.fromJson(json['data'])
    );
  }
}

