class HomeWorkPlaceInfoItem {
  num id;
  String businessName;
  String ownerName;
  String businessType;
  String businessPhoneNumber;
  String reallyLocation;

  HomeWorkPlaceInfoItem(
      this.id,
      this.businessName,
      this.ownerName,
      this.businessType,
      this.businessPhoneNumber,
      this.reallyLocation);

  factory HomeWorkPlaceInfoItem.fromJson(Map<String, dynamic> json) {
    return HomeWorkPlaceInfoItem(
      json['id'],
      json['businessName'],
      json['ownerName'],
      json['businessType'],
      json['businessPhoneNumber'],
      json['reallyLocation'],
    );
  }
}
