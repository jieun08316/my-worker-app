class LocationResponse {
  String msg;
  String code;

  LocationResponse(
      this.msg,
      this.code
      );

  factory LocationResponse.fromJson(Map<String, dynamic> json) {
    return LocationResponse(
        json['msg'],
        json['code']
    );
  }
}