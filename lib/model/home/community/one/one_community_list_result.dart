import 'package:my_worker_app/model/home/community/one/one_community_list.dart';

class OneCommunityListResult {
  num totalCount;
  num totalPage;
  num currentPage;
  List<OneCommunityList>? list;

  OneCommunityListResult(this.totalCount, this.totalPage, this.currentPage, this.list);

  factory OneCommunityListResult.fromJson(Map<String, dynamic> json) {
    return OneCommunityListResult(
      json['totalCount'],
      json['totalPage'],
      json['currentPage'],
      json['list'] != null ? (json['list'] as List).map((e) => OneCommunityList.fromJson(e)).toList() : [],
    );
  }
}