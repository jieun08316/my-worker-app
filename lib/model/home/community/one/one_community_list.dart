class OneCommunityList {
  num id;
  String title;
  String content;
  String boardImgUrl;

  OneCommunityList(
      this.id,
      this.title,
      this.content,
      this.boardImgUrl,
  );

  factory OneCommunityList.fromJson(Map<String, dynamic> json) {
    return OneCommunityList(
      json['id'],
      json['title'],
      json['content'],
      json['boardImgUrl'],
    );
  }
}