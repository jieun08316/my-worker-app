
import 'package:my_worker_app/model/home/community/one/one_community_comment.dart';

class OneCommunityDetail {
  // 글 부분
  num id;
  String title;
  String content;
  String boardImgUrl;
  String dateBoard;

  // 댓글 부분
  List<OneCommunityComment> comments;

  OneCommunityDetail(
      this.id,
      this.title,
      this.content,
      this.boardImgUrl,
      this.dateBoard,
      this.comments
      );

  factory OneCommunityDetail.fromJson(Map<String, dynamic> json) {
    return OneCommunityDetail(
      json['id'],
      json['title'],
      json['content'],
      json['boardImgUrl'],
      json['dateBoard'],
      json['comments'] != null ? (json['comments'] as List).map((e) => OneCommunityComment.fromJson(e)).toList() : [],
    );
  }
}