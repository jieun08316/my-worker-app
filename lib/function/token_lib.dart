import 'package:flutter/material.dart';
import 'package:my_worker_app/pages/member/page_login.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TokenLib {
  // SharedPreference: 영구 저장소
  static Future<String?> getMemberToken() async { // SharedPreferences 영구저장소
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('memberToken');
  } // 토큰을 영구저장소에서 꺼내오겠다 - 영구저장소는 따로 추가해준 패키지이기 때문에 거기에 들려서 가져와야 하기 때문에 기다려야 한다
  static Future<String?> getMemberName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('memberName');
  }
  static void setMemberToken(String memberToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberToken', memberToken);
  } // set부터 해야 token이 생기므로 set부터 하고 get을 해야한다
  static void setMemberName(String memberName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberName', memberName);
  }

  static void logout(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.clear();

    // pushAndRemoveUntil: 현재까지 쌓은 페이지 지우고 페이지 새로 넣기
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (BuildContext context) => const PageLogin()),
            (route) => false
    );
  }
}
