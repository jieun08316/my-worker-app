// import 'package:geolocator/geolocator.dart';
//
// Future<bool> MiddlewareCheckLocation(double targetLatitude, double targetLongitude) async {
//   bool isLocationMatched = false;
//
//   try {
//     // 현재 위치 가져오기
//     Position currentPosition = await Geolocator.getCurrentPosition(
//         desiredAccuracy: LocationAccuracy.best);
//
//     // // 현재 위치와 설정한 위치 비교
//     double currentLatitude = currentPosition.latitude;
//     double currentLongitude = currentPosition.longitude;
//
//     // 일정 범위 내의 위치만 일치로 판단 (예: 0.001 정도의 오차 허용)
//     double accuracy = 0.001;
//     bool isLatitudeMatched =
//         (currentLatitude - targetLatitude).abs() < accuracy;
//     bool isLongitudeMatched =
//         (currentLongitude - targetLongitude).abs() < accuracy;
//
//     if (isLatitudeMatched && isLongitudeMatched) {
//       isLocationMatched = true;
//       print('위치 체크 성공');
//
//     }
//   } catch (e) {
//     print('위치 확인 에러: $e');
//   }
//
//   return isLocationMatched;
// }