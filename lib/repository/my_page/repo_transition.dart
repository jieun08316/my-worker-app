import 'package:dio/dio.dart';
import 'package:my_worker_app/config/config_api.dart';
import 'package:my_worker_app/function/token_lib.dart';
import 'package:my_worker_app/model/common_result.dart';
import 'package:my_worker_app/model/home/transition/transition_detail_result.dart';
import 'package:my_worker_app/model/home/transition/transition_list.dart';
import 'package:my_worker_app/model/home/transition/transition_list_result.dart';
import 'package:my_worker_app/model/home/transition/transition_put_request.dart';
import 'package:my_worker_app/model/home/transition/transition_regist_request.dart';

class RepoTransition {

  // C
  Future<CommonResult> setTransition(TransitionRegistRequest request,num businessId) async {

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    // 주소 설정
    String _baseUrl = '$apiUri/v1/transition/new/business-id/{businessId}';
    // /v1/transition/new/business-id/{businessId}

    final response = await dio.post(
        _baseUrl.replaceAll('{businessId}', businessId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  // 페이지 처음 들어왔을 때부터 데이터 다 불러오도록
  Future<TransitionListResult> getList(int year, int month) async {

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    String _baseUrl = '$apiUri/v1/transition/calendar?year=${year}&month=${month}';
    // /v1/transition/calender
    // /v1/transition/calender?year=2024&month=04

    final response = await dio.get(
        _baseUrl,
        options: Options(
          // 데이터 없을 경우 다른 곳으로 안내 => false
            followRedirects: false,
            validateStatus: (status) {
              // 성공했을 경우 처리
              return status == 200;
            }
        )
    );

    print(response.data);
    return TransitionListResult.fromJson(response.data);

  }

  // 날짜 선택하면 리스트 보이기
  Future<TransitionListResult> getDetail(String localDate) async {

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    String _baseUrl = '$apiUri/v1/transition/calendar/day?localDate={localDate}';
    // /v1/transition/calender/day
    // /v1/transition/calendar/day?localDate=2024-04-27

    final response = await dio.get(
        _baseUrl.replaceAll('{localDate}', localDate),
        options: Options(
          // 데이터 없을 경우 다른 곳으로 안내 => false
            followRedirects: false,
            validateStatus: (status) {
              // 성공했을 경우 처리
              return status == 200;
            }
        )
    );


    print(response.data);
    return TransitionListResult.fromJson(response.data);

  }

  // 미처리 버튼 누르면 처리로 상태 변경
  Future<TransitionList> putButton (num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/v1/transition/finish/transition-id/{transitionId}';
    // /v1/transition/finish/transition-id/{transitionId}

    final response = await dio.put(
        _baseUrl.replaceAll('{transitionId}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return TransitionList.fromJson(response.data);
  }

  // U 인수인계 수정
  Future<CommonResult> putTransition (TransitionPutRequest request, num id) async {

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    // 주소 설정
    String _baseUrl = '$apiUri/v1/transition/change/transition-id/{transitionId}';
    // /v1/transition/change/transition-id/{transitionId}

    final response = await dio.put(
        _baseUrl.replaceAll('{transitionId}', id.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  // 단수 R 구현
  Future<TransitionDetailResult> getTransition (num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/v1/transition/detail/transition-id/{transitionId}';
    // /v1/transition/detail/transition-id/{transitionId}

    final response = await dio.get(
        _baseUrl.replaceAll('{transitionId}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    print(response.data);
    return TransitionDetailResult.fromJson(response.data);
  }

}