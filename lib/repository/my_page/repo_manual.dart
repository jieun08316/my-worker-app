import 'package:dio/dio.dart';
import 'package:my_worker_app/config/config_api.dart';
import 'package:my_worker_app/function/token_lib.dart';
import 'package:my_worker_app/model/common_result.dart';
import 'package:my_worker_app/model/home/manual/manual_detail_result.dart';
import 'package:my_worker_app/model/home/manual/manual_list_result.dart';
import 'package:my_worker_app/model/home/manual/manual_regist_request.dart';

class RepoManual {

  // 복수 R 구현
  Future<ManualListResult> getList(num businessId,{int page = 1}) async {
    Dio dio = Dio();

    String? token = await TokenLib.getMemberToken();

    String _baseUrl = '$apiUri/v1/manual/all/business/{pageNum}/business-id/{businessId}'.replaceAll('{pageNum}', page.toString());
    // /v1/manual/all/business/{pageNum}/business-id/{businessId}

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(
        _baseUrl.replaceAll('{pageNum}', page.toString()).replaceAll('{businessId}', businessId.toString()),
        options: Options(
          // 데이터 없을 경우 다른 곳으로 안내 => false
            followRedirects: false,
            validateStatus: (status) {
              // 성공했을 경우 처리
              return status == 200;
            }
        )
    );

    return ManualListResult.fromJson(response.data);

  }

  // 단수 R 구현
  Future<ManualDetailResult> getProduct (num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/v1/manual/detail/manual-id/{manualId}';
    // /v1/manual/detail/manual-id/{manualId}

    final response = await dio.get(
        _baseUrl.replaceAll('{manualId}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    print(response.data);
    return ManualDetailResult.fromJson(response.data);
  }

  // C
  Future<CommonResult> setManual(ManualRegistRequest request) async {

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    // 주소 설정
    String _baseUrl = '$apiUri/v1/manual/new/';
    // /v1/manual/new/

    final response = await dio.post(
        _baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);

  }

}