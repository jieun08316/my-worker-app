import 'package:dio/dio.dart';
import 'package:my_worker_app/config/config_api.dart';
import 'package:my_worker_app/function/token_lib.dart';
import 'package:my_worker_app/model/common_result.dart';
import 'package:my_worker_app/model/home/community/one/one_community_detail_result.dart';
import 'package:my_worker_app/model/home/community/one/one_community_list_result.dart';
import 'package:my_worker_app/model/home/community/one/one_community_put_request.dart';
import 'package:my_worker_app/model/home/community/one/one_community_regist_request.dart';

class RepoBoard {

// 복수 R 구현: 같이 쓰는 게시판
  Future<OneCommunityListResult> getList({int page = 1}) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/v1/board/all/integration/{pageNum}'.replaceAll('{pageNum}', page.toString());
    // /v1/board/all/integration/{pageNum}

    final response = await dio.get(
        _baseUrl.replaceAll('{pageNum}', page.toString()),
        options: Options(
          // 데이터 없을 경우 다른 곳으로 안내 => false
            followRedirects: false,
            validateStatus: (status) {
              // 성공했을 경우 처리
              return status == 200;
            }
        )
    );

    return OneCommunityListResult.fromJson(response.data);

  }

  // 복수 R 구현: 알바생 혼자 쓰는 게시판
  Future<OneCommunityListResult> getListOne({int page = 1}) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/v1/board/all/job/{pageNum}'.replaceAll('{pageNum}', page.toString());
    // /v1/board/all/job/{pageNum}

    final response = await dio.get(
        _baseUrl.replaceAll('{pageNum}', page.toString()),
        options: Options(
          // 데이터 없을 경우 다른 곳으로 안내 => false
            followRedirects: false,
            validateStatus: (status) {
              // 성공했을 경우 처리
              return status == 200;
            }
        )
    );

    return OneCommunityListResult.fromJson(response.data);

  }

  // 단수 R 구현:알바생 혼자: 글
  Future<OneCommunityDetailResult> getProductOneContent (num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/v1/board/detail/board-id/{boardId}';
    // /v1/board/detail/board-id/{boardId}

    final response = await dio.get(
        _baseUrl.replaceAll('{boardId}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return OneCommunityDetailResult.fromJson(response.data);
  }

  // 단수 R 구현: 같이: 글
  Future<OneCommunityDetailResult> getProduct (num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/v1/board/detail/board-id/{boardId}';
    // /v1/board/detail/board-id/{boardId}

    final response = await dio.get(
        _baseUrl.replaceAll('{boardId}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return OneCommunityDetailResult.fromJson(response.data);
  }

  // C
  Future<CommonResult> setBoard(OneCommunityRegistRequest request) async {

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    // 주소 설정
    String _baseUrl = '$apiUri/v1/board/new';
    // /v1/board/new

    final response = await dio.post(
        _baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  // D
  Future<OneCommunityDetailResult> deleteBoard (num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/v1/board/del/board-id/{boardId}';
    // /v1/board/del/board-id/{boardId}

    final response = await dio.delete(
        _baseUrl.replaceAll('{boardId}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return OneCommunityDetailResult.fromJson(response.data);
  }
  // U 알바생 커뮤니티 글 수정
  Future<CommonResult> putBoard(OneCommunityPutRequest request, num id) async {

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    // 주소 설정
    String _baseUrl = '$apiUri/v1/board/put/my-board/board-id/{boardId}';
    // /v1/board/put/my-board/board-id/{boardId}

    final response = await dio.put(
        _baseUrl.replaceAll('{boardId}', id.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }





}