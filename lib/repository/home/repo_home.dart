import 'package:dio/dio.dart';
import 'package:my_worker_app/config/config_api.dart';
import 'package:my_worker_app/function/token_lib.dart';
import 'package:my_worker_app/model/home/home_work_place_info_result.dart';
import 'package:my_worker_app/model/home/location_result.dart';
import 'package:my_worker_app/model/main/my_work_attendance_list_result.dart';

class RepoHome {
//출퇴근 리스트 보기
  Future<MyWorkAttendanceListResult> getAttendanceList(num businessId,{int page = 1}) async {
    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';
    String _baseUrl =
        '$apiUri/v1/schedule/all/member/business-id/{businessId}/{pageNum}'.replaceAll('{pageNum}', page.toString());
    // /v1/schedule/all/member/business-id/{businessId}/{pageNum}
    final response = await dio.get(
        _baseUrl.replaceAll('{businessId}', businessId.toString()),
            options: Options(
              // 데이터 없을 경우 다른 곳으로 안내 => false
                followRedirects: false,
                validateStatus: (status) {
                  // 성공했을 경우 처리
                  return status == 200;
                }));
    print(response);

    return MyWorkAttendanceListResult.fromJson(response.data);
  }

//

//R
  ///해당 회원이 근로 계약 되어 있는 사업장 불러 오기 ,리스트
  Future<HomeWorkPlaceInfoResult> getItem(num businessId) async {
    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';
    String _baseUrl = '$apiUri/v1/business/detail/business-id/{businessId}';

    final response = await dio.get(
        _baseUrl.replaceAll('{businessId}', businessId.toString()),
        options: Options(
            // 데이터 없을 경우 다른 곳으로 안내 => false
            followRedirects: false,
            validateStatus: (status) {
              // 성공했을 경우 처리
              return status == 200;
            }));
    return HomeWorkPlaceInfoResult.fromJson(response.data);
  }

//longitude latitude

//C
  /// 출근 등록 🌸 보류 🌸

  Future<LocationResult> doAttendance(num businessId) async {
    Dio dio = Dio();
    String _baseUrl = '$apiUri/v1/schedule/new/business-id/{businessId}';
    // /v1/schedule/new/business-id/{businessId}
    final response = await dio.post(
        _baseUrl.replaceAll('{businessId}', businessId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    print(response);

    return LocationResult.fromJson(response.data);
  }

//
}
