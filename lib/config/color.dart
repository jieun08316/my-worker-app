import 'dart:ui';

const Color colorDark = Color.fromRGBO(17, 45, 78, 1.0);
const Color colorNormal = Color.fromRGBO(63, 114, 175, 1.0);
const Color colorLight = Color.fromRGBO(219, 226, 239, 1.0);
const Color colorGrey = Color.fromRGBO(150, 150, 150, 1.0);
const Color colorBaseBlack = Color.fromRGBO(100, 100, 100, 1.0);
