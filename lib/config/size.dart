const double smallGap = 5.0;
const double mediumGap = 10.0;
const double largeGap = 20.0;
const double xlargeGap = 100.0;

const double fontSizeMicro = 10;
const double fontSizeSmall = 12;
const double fontSizeMedium = 14;
const double fontSizeLarge = 16;
const double fontSizeXlarge = 18;
const double fontSizeXlarge2 = 25;
const double fontSizeXXlarge = 30;

const double circularRadiusSmall= 8.0;
const double circularRadiusBase =20.0;
